CREATE SEQUENCE AGENCIA_SEQ;
CREATE SEQUENCE BANCOS_SEQ; 
CREATE SEQUENCE CLIENTES_SEQ;
CREATE SEQUENCE CONSOLIDACAO_SEQ; 
CREATE SEQUENCE CONTA_SEQ;
CREATE SEQUENCE DADOS_SEQ;
CREATE SEQUENCE ENDERECO_SEQ; 
CREATE SEQUENCE GERENTE_SEQ;
CREATE SEQUENCE MOVIMENTACAO_SEQ;
CREATE SEQUENCE PESSOA_SEQ;
CREATE SEQUENCE TIPO_DA_CONTA_SEQ; 
CREATE SEQUENCE TIPO_GERENTE_SEQ;

-- INSERT BANCOS
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '011','Banco Alfa');
INSERT INTO BANCOS(ID_BANCOS,ID_DADOS) 
    VALUES 
          ( BANCOS_SEQ.NEXTVAL,(SELECT ID_DADOS FROM DADOS WHERE NOME LIKE 'Banco Alfa'));

-- insert agencia 001
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '0001','Agência'); 

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','55','loja 1','11111111','NA','NA','NA','Brasil');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Alfa'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '0001' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '55' AND PAIS LIKE 'Brasil' ),
    9
);

-- AGENCIA 002
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '0002','Agência'); 

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Casa','122','Rua Testing','11111111',
'Between Hyde and Powell Streets','San Francisco','California','EUA');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Alfa'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '0002' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '122' AND PAIS LIKE 'EUA' ),
    10
);

-- AGENCIA 003
   
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '0101','Agência'); 

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Londres - Rua Tesing','525','loja 1','11111111','Croydon','Londres','Boroughs','England');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Alfa'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '0101' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '525' AND PAIS LIKE 'England' ),
    11
);


-- INSERT BANCOS 2
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '241','Banco Beta');
INSERT INTO BANCOS(ID_BANCOS,ID_DADOS) 
    VALUES 
          ( BANCOS_SEQ.NEXTVAL,(SELECT ID_DADOS FROM DADOS WHERE NOME LIKE 'Banco Beta'));

-- insert agencia 001

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','55','loja 2','11111111','NA','NA','NA','Brasil');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Beta'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '0001' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '55' AND PAIS LIKE 'Brasil' AND COMPLEMENTO LIKE 'loja 2' ),
    12
);

-- INSERT BANCO 3
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '307','Banco Omega');
INSERT INTO BANCOS(ID_BANCOS,ID_DADOS) 
    VALUES 
          ( BANCOS_SEQ.NEXTVAL,(SELECT ID_DADOS FROM DADOS WHERE NOME LIKE 'Banco Omega'));
SELECT * FROM DADOS;
-- insert agencia 001

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','55','loja 3','11111111','NA','NA','NA','Brasil');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Omega'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '0001' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE PAIS LIKE 'Brasil' AND COMPLEMENTO LIKE 'loja 3'),
    13
);

-- AGENCIA 002
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '8761','Agência'); 

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Itu - Rua do meio','2233','Rua Testing','11111111',
'Qualquer','Itu','São Paulo','Brasil');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Omega'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '8761' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '2233' AND PAIS LIKE 'Brasil' ),
    14
);

-- AGENCIA 003
   
INSERT INTO DADOS(ID_DADOS, CODIGO, NOME) VALUES (DADOS_SEQ.NEXTVAL, '4567','Agência'); 

INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO,SALDO_ATUAL,SAQUES,DEPOSITOS,NUMERO_DE_CORRENTISTAS)
    VALUES(CONSOLIDACAO_SEQ.NEXTVAL, 0.0,0.0,0.0,0);
    
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,CEP,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Hermana - Rua do boca','222','loja 1','11111111','Caminito','Buenos Aires','Buenos Aires','Argentina');

INSERT INTO AGENCIA(ID_AGENCIA,ID_BANCOS,ID_DADOS,ID_ENDERECO,ID_CONSOLIDACAO) VALUES (
    AGENCIA_SEQ.NEXTVAL,
    (SELECT BANCOS.ID_BANCOS FROM BANCOS INNER JOIN DADOS ON BANCOS.ID_DADOS = DADOS.ID_DADOS WHERE DADOS.NOME LIKE 'Banco Omega'),
    (SELECT ID_DADOS FROM DADOS WHERE CODIGO LIKE '4567' ),
    (SELECT ID_ENDERECO FROM ENDERECO WHERE NUMERO = '222' AND PAIS LIKE 'Argentina' ),
    15
);