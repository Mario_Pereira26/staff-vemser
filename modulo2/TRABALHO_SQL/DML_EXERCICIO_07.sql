-- Bancos por Pais
SELECT DISTINCT BANCOS.ID_BANCOS, DADOS.NOME, DADOS.CODIGO, ENDERECO.PAIS FROM BANCOS INNER JOIN AGENCIA 
ON BANCOS.ID_BANCOS = AGENCIA.ID_BANCOS INNER JOIN DADOS
ON AGENCIA.ID_DADOS = DADOS.ID_DADOS INNER JOIN ENDERECO
ON AGENCIA.ID_ENDERECO = ENDERECO.ID_ENDERECO WHERE PAIS LIKE 'EUA';

--Completo de agência (Dados de agência, correntistas, clientes e dados dos clientes)

SELECT 
       TIPO_DA_CONTA.TIPO,
       DADOS.CODIGO, DADOS.NOME, CONTA.CODIGO,
       PESSOA.NOME,
       PESSOA.CPF,
       PESSOA.ESTADO_CIVIL,
       PESSOA.DATA_NASC,
       ENDERECO.LOGRADOURO,ENDERECO.NUMERO,ENDERECO.COMPLEMENTO,
       ENDERECO.CEP, ENDERECO.BAIRRO, ENDERECO.CIDADE,ENDERECO.ESTADO,ENDERECO.PAIS 
FROM AGENCIA INNER JOIN CONTA ON AGENCIA.ID_AGENCIA = CONTA.ID_AGENCIA
INNER JOIN DADOS ON DADOS.ID_DADOS = AGENCIA.ID_DADOS
INNER JOIN CONTA_X_CLIENTES ON CONTA.ID_CONTA = CONTA_X_CLIENTES.ID_CONTA
INNER JOIN TIPO_DA_CONTA ON TIPO_DA_CONTA.ID_TIPO = CONTA.ID_TIPO
INNER JOIN CLIENTES ON CONTA_X_CLIENTES.ID_CLIENTES = CLIENTES.ID_CLIENTES
INNER JOIN PESSOA ON CLIENTES.ID_PESSOA = PESSOA.ID_PESSOA
INNER JOIN ENDERECO ON PESSOA.ID_ENDERECO = ENDERECO.ID_ENDERECO;

-- Geral de clientes cadastrados no nosso sistema (lista de todos clientes com seus dados)

SELECT  PESSOA.NOME,PESSOA.CPF,PESSOA.ESTADO_CIVIL,PESSOA.DATA_NASC,
        ENDERECO.LOGRADOURO, ENDERECO.NUMERO, ENDERECO.COMPLEMENTO,
        ENDERECO.CEP, ENDERECO.BAIRRO, ENDERECO.CIDADE, ENDERECO.ESTADO, ENDERECO.PAIS
FROM CLIENTES INNER JOIN PESSOA 
ON CLIENTES.ID_PESSOA = PESSOA.ID_PESSOA INNER JOIN ENDERECO
ON PESSOA.ID_ENDERECO = ENDERECO.ID_ENDERECO;