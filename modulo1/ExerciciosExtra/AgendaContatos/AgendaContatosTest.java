import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {
    
    @Test
    public void adicionarContatoEPesquisa( ){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Mario","99999-9999");
        String resultado = agenda.consultar("Mario");
        assertEquals("99999-9999",resultado);
    }
    
    @Test
    public void adicionarContatoEPesquisaTelefone( ){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Mario","99999-9999");
        String resultado = agenda.consultarPeloTelefone("99999-9999");
        assertEquals("Mario",resultado);
    }

    @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Mario","99999-9999");
        agenda.adicionar("Mario Henrique","99999-8888");
        String separador = System.lineSeparator();
        String resultado = String.format("Mario,99999-9999%sMario Henrique,99999-8888%s",separador,separador);

        assertEquals(resultado,agenda.csv());
    }
}
