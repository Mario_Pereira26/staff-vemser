import java.util.*;
public class ExercitoQueAtaca extends ExercitoElfo {
    private OrdemDeAtaque estrategia;
    
    public ExercitoQueAtaca( OrdemDeAtaque estrategia ){
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia(OrdemDeAtaque estrategia) {
        this.estrategia = estrategia;
    }
    
    public void atacar(ArrayList<Dwarf> dwarfs) {
        ArrayList <Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.exercitoElfo);
        for(Elfo elfo : ordem){
            for( Dwarf dwarf : dwarfs){
                elfo.atirarFlecha(dwarf);
            }
        }
    }
}
