import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    @Test
    public void elfoNoturnoGanhaTriploDeXp(){
        ElfoNoturno dralvius = new ElfoNoturno("Dralvius");
        dralvius.atirarFlecha(new Dwarf("Balin"));
        assertEquals( 3, dralvius.getExperiencia() );
    }
    
    @Test
    public void elfoNoturnoPerde15DeVidaAoAtirarFlecha(){
        ElfoNoturno dralvius = new ElfoNoturno("Dralvius");
        
        assertEquals(100, dralvius.getVida(), 1e-9);
        dralvius.atirarFlecha(new Dwarf("Balin"));
        assertEquals(85, dralvius.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        ElfoNoturno dralvius = new ElfoNoturno("Dralvius");
        dralvius.getInventario().obter(1).setQuantidade(1000);
        
        assertEquals(100, dralvius.getVida(), 1e-9);
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        dralvius.atirarFlecha(new Dwarf("Balin"));
        assertEquals(.0, dralvius.getVida(), 1e-9);
        assertEquals(Status.MORTO, dralvius.getStatus());
    }
    
    @Test
    public void teste(){
    //getOrdemDeAtaque(ArrayList<Elfo> atacantes)
        
    }
}
