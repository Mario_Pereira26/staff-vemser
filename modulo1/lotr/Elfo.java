import java.util.*;
public class Elfo extends Personagem implements Atacar{
    private int indiceFlecha;
    private static int qtdElfos;

    {
        this.indiceFlecha = 1;
    }
    
    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }
    
    public void atacar(Dwarf dwarf){
        this.atirarFlecha(dwarf);
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int qtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirarFlecha() {
        return this.getQtdFlechas() > 0;
    }
    
    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getQtdFlechas();
        if( podeAtirarFlecha() ) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //this.experiencia = experiencia + 1;
            aumentarXP();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    
    public String imprimirNomeDaClasse(){
        return "Elfo";
    };
}
