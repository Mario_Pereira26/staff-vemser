import java.util.*;
public class AtaqueComFlechas implements EstrategiaDeAtaque {
   
    protected final static ArrayList<Class> ELFO_NOTURNO = new ArrayList<>(
        Arrays.asList(
            ElfoNoturno.class
        )
    );
    
    public void ordenarPorFlechas(ArrayList<Elfo> ordem) {
        boolean troca;
        Elfo elfoAuxiliar;
        do{
            troca = false;
            for(int i = 0 ; i < ordem.size()-1 ; i++){
                if(ordem.get(i).getQtdFlechas() < ordem.get(i+1).getQtdFlechas()){
                    elfoAuxiliar = ordem.get(i);
                    ordem.set(i, ordem.get(i+1));
                    ordem.set(i+1, elfoAuxiliar);
                    troca = true;
                }  
            }
        }while(troca);
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> arrayElfoAuxiliar = new ArrayList<>();
        arrayElfoAuxiliar.addAll(atacantes);
        
        ArrayList<Elfo> ordemAtaque = new ArrayList<>();
        int contNoturno = 0, contVerde = 0;
        for(Elfo elfo : atacantes){
            
            if( elfo.getVida() > 0 && elfo.getQtdFlechas() > 0 ) {
                if(elfo.getClass().equals(this.ELFO_NOTURNO.get(0))){
                    contNoturno++;
                    
                }else{
                    contVerde++;
                }
                ordemAtaque.add(elfo);
                arrayElfoAuxiliar.remove(elfo);
            }
        }

        if ((contNoturno > 0) && (contNoturno <=  (double)ordemAtaque.size() * 0.3)) {
            this.ordenarPorFlechas(ordemAtaque);
            return ordemAtaque;
        }
            
        return null;
    }
}
