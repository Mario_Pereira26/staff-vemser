import java.util.*;

public class ExercitoElfo {
    ArrayList<Elfo> exercitoElfo = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    public void alistar(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        
        if(podeAlistar){
            exercitoElfo.add(elfo);
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get(elfo.getStatus());
            if(elfoDeUmStatus == null){
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDeUmStatus);
            }
            elfoDeUmStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> getElfos(){
        return this.exercitoElfo;
    }
    
    public ArrayList<Elfo> buscar(Status status){
        return this.porStatus.get(status);
    }
}
