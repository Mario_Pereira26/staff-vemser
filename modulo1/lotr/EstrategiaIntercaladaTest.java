import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaIntercaladaTest {
   
    @Test
    public void exercitoIntercaladoComecandoComElfoVerde() {
        EstrategiaIntercalada estrategia = new EstrategiaIntercalada();
        
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2= new ElfoNoturno("Night 2");
        Elfo green1 = new ElfoVerde("green 1");
        Elfo green2 = new ElfoVerde("gren 2");
        
        ArrayList<Elfo> elfoEntrada = new ArrayList<>(
            Arrays.asList(green1,green2,night1,night2)
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(green1,night1,green2,night2)
        );
        
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(elfoEntrada);
        assertEquals( esperado,obtido );
    }
    
    @Test
    public void exercitoIntercaladoComecandoComElfoNoturno() {
        EstrategiaIntercalada estrategia = new EstrategiaIntercalada();
        
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2= new ElfoNoturno("Night 2");
        Elfo green1 = new ElfoVerde("green 1");
        Elfo green2 = new ElfoVerde("gren 2");
        
        ArrayList<Elfo> elfoEntrada = new ArrayList<>(
            Arrays.asList(night1,night2,green1,green2)
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(night1,green1,night2,green2)
        );
        
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(elfoEntrada);
        assertEquals( esperado,obtido );
    }
    
   @Test
    public void ataqueElfosIntercaladoPrimeiroElfoVerde(){
        ExercitoElfo exercito = new ExercitoElfo();
        EstrategiaIntercalada estrategiaAtaqueIntercalado = new EstrategiaIntercalada();
        
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        ElfoVerde elfoVerde2 = new ElfoVerde("Celebron 2");
        ElfoVerde elfoVerde3 = new ElfoVerde("Celebron 3");
        
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dralvius");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Dralvius 2");
        ElfoNoturno elfoNoturno3 = new ElfoNoturno("Dralvius 3");
        
        ArrayList<Elfo> arrayEsperado = new ArrayList<>(
            Arrays.asList(
                elfoVerde,elfoNoturno,elfoVerde2,elfoNoturno2,elfoVerde3,elfoNoturno3
            )
        );
        exercito.alistar(elfoVerde); 
        exercito.alistar(elfoVerde2); 
        exercito.alistar(elfoVerde3);
        
        exercito.alistar(elfoNoturno); 
        exercito.alistar(elfoNoturno2); 
        exercito.alistar(elfoNoturno3); 
        
        
        assertEquals( arrayEsperado,estrategiaAtaqueIntercalado.getOrdemDeAtaque( exercito.getElfos() ) );
    }
    
    @Test
    public void ataqueElfosIntercaladoPrimeiroElfoNoturno(){
        ExercitoElfo exercito = new ExercitoElfo();
        EstrategiaIntercalada estrategiaAtaqueIntercalado = new EstrategiaIntercalada();
        
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dralvius");
        
        ArrayList<Elfo> arrayEsperado = new ArrayList<>(
            Arrays.asList(
                elfoNoturno,elfoVerde,elfoNoturno,elfoVerde,elfoNoturno,elfoVerde
            )
        );
        for(int i = 0 ; i < 6 ; i++){
            if(i >= 3){
                exercito.alistar(elfoVerde); 
            }else{
                exercito.alistar(elfoNoturno); 
            }
        }
        
        assertEquals( arrayEsperado,estrategiaAtaqueIntercalado.getOrdemDeAtaque( exercito.getElfos() ) );
    }
}
