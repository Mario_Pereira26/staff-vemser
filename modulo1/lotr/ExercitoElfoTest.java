import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfoTest {
    
    @Test
    public void podeAlistarElfoVerde(){
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
    @Test
    public void podeAlistarElfoNoturno(){
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoNoturno elfoNoturno = new ElfoNoturno("Night Legolas");
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }
    
    @Test
    public void naoPodeAlistarElfoDaLuz(){
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Luz Legolas");
        exercito.alistar(elfoDaLuz);
        assertFalse(exercito.getElfos().contains(elfoDaLuz));
    }
    
    @Test
    public void alistarUmElfoVerdeOutroElfoNoturno(){
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dralvius");
        ArrayList<Elfo> arrayElfo = new ArrayList<>(
            Arrays.asList(
                elfoVerde,elfoNoturno
            )
        );
        
        exercito.alistar(elfoVerde); 
        exercito.alistar(elfoNoturno); 
        
        assertEquals(arrayElfo,exercito.getElfos());
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
    @Test
    public void buscarListaElfoPorStatus(){
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dralvius");
        ArrayList<Elfo> arrayElfo = new ArrayList<>(
            Arrays.asList(
                elfoVerde,elfoNoturno
            )
        );
        
      
        exercito.alistar(elfoVerde);
        exercito.alistar(elfoNoturno);
        
        assertEquals(arrayElfo,exercito.buscar(Status.RECEM_CRIADO));
    }
}
