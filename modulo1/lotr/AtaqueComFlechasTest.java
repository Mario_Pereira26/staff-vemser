import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueComFlechasTest {
    
    @Test
    public void ComFlechasAcima30PorcentoDoTotalElfosNoturnos(){
        ExercitoElfo exercito = new ExercitoElfo();
        AtaqueComFlechas estrategiaAtaqueComFlechas = new AtaqueComFlechas();
        
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dralvius");
        elfoNoturno.getInventario().buscar("Flecha").setQuantidade(0);
        
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Dralvius 2");
        elfoNoturno2.getInventario().buscar("Flecha").setQuantidade(12);
        
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        elfoVerde.getInventario().buscar("Flecha").setQuantidade(0);
        ElfoVerde elfoVerde2 = new ElfoVerde("Celebron 2");
        elfoVerde2.getInventario().buscar("Flecha").setQuantidade(40);
        ElfoVerde elfoVerde3 = new ElfoVerde("Celebron 3");
        elfoVerde3.getInventario().buscar("Flecha").setQuantidade(50);
        
       
        exercito.alistar(elfoVerde); 
        exercito.alistar(elfoVerde2); 
        exercito.alistar(elfoVerde3);
        
        exercito.alistar(elfoNoturno); 
        exercito.alistar(elfoNoturno2);
        
        ArrayList<Elfo> arrayEsperado = new ArrayList<>(
            Arrays.asList(
                elfoVerde3,elfoVerde2,elfoNoturno2
            )
        );

        assertEquals( arrayEsperado,estrategiaAtaqueComFlechas.getOrdemDeAtaque(exercito.getElfos()) );
        
    }
}


