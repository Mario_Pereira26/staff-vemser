class Pokemon {// eslint-disable-line no-unused-vars
  constructor( objDaAPI ) {
    this._id = objDaAPI.id;
    this._nome = objDaAPI.name;
    this._imagem = objDaAPI.sprites.front_default;
    this._altura = objDaAPI.height;
    this._peso = objDaAPI.weight;
    this._listaTipos = objDaAPI.types;
    this._estatisticas = objDaAPI.stats;
  }

  get id() {
    return this._id;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm`;
  }

  get peso() {
    return `${ this._peso / 10 }`;
  }

  get listaTipos() {
    return this._listaTipos.map( e => e );
  }

  get estatisticas() {
    return this._estatisticas;
  }
}
