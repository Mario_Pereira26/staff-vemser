const pokeApi = new PokeApi();
let pokemonExibido;
const pokemonAleatorios = [];

const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const removerTagsLi = ( div ) => {
    while ( div.hasChildNodes() ) {
      div.removeChild( div.firstChild )
    }
  }

  const inserirTituloLista = ( nomeClasse, texto ) => {
    const titulo = document.querySelector( nomeClasse );
    titulo.innerText = texto;
  }

  const inserirListaTipo = () => {
    const lista = dadosPokemon.querySelector( '.lista' );
    let li;
    let span;
    removerTagsLi( lista );
    inserirTituloLista( '.tituloLista', 'Tipo:' );
    pokemon.listaTipos.map( item => {
      li = document.createElement( 'li' );
      span = document.createElement( 'span' );
      li.appendChild( span );
      lista.appendChild( li );
      span.innerText = item.type.name;
      return true;
    } );
  }

  const inserirListaEstatitica = () => {
    const listaEstatisticas = dadosPokemon.querySelector( '.estatisticas' );
    let li;
    let span;
    removerTagsLi( listaEstatisticas );
    inserirTituloLista( '.tituloEstatistica', 'Estatistica:' );
    pokemon.estatisticas.map( item => {
      li = document.createElement( 'li' );
      span = document.createElement( 'span' );
      li.appendChild( span );
      listaEstatisticas.appendChild( li );
      li.innerText = `${ item.stat.name } base: ${ item.base_stat }% `;
      return true;
    } );
  }

  const inserirDadosPokemon = () => {
    const id = dadosPokemon.querySelector( '.id' );
    const nome = dadosPokemon.querySelector( '.nome' );
    const altura = dadosPokemon.querySelector( '.altura' );
    const peso = dadosPokemon.querySelector( '.peso' );
    const imgPokemon = document.querySelector( '.thumb' );
    imgPokemon.src = pokemon.imagem;
    imgPokemon.alt = `Pokemon selecionado ${ pokemon.name }`;
    pokemonExibido = pokemon.id || 0;
    id.innerText = `Id: ${ pokemon.id }`;
    nome.innerText = `Nome: ${ pokemon.nome }`;
    altura.innerText = `Altura: ${ pokemon.altura }`;
    peso.innerText = `Peso: ${ pokemon.peso } kg`;
    inserirListaTipo();
    inserirListaEstatitica();
  }
  inserirDadosPokemon();
}
const ocultarInfo = ( visivel ) => {
  if ( visivel === 'sim' ) {
    document.getElementById( 'dadosBase' ).style.display = 'none';
    document.getElementById( 'dadosEstatistica' ).style.display = 'none';
  } else {
    document.getElementById( 'dadosBase' ).style.display = 'block';
    document.getElementById( 'dadosEstatistica' ).style.display = 'block';
  }
}

const inputId = document.querySelector( '.campo' );
inputId.addEventListener( 'blur', function () {
  const regra = /^[0-9]+$/;
  if ( this.value.match( regra ) ) {
    if ( this.value !== pokemonExibido ) {
      pokeApi
        .buscarEspecifico( this.value )
        .then( pokemon => {
          const poke = new Pokemon( pokemon );
          renderizar( poke );
        } );
    }
    ocultarInfo( 'nao' );
    document.querySelector( '.campo' ).value = '';
  } else {
    const imgPokemon = document.querySelector( '.thumb' );
    imgPokemon.src = '';
    imgPokemon.alt = 'Digite um id válido!';
    ocultarInfo( 'sim' );
  }
}, false );

const botaoSorte = document.querySelector( '.sorte' );
botaoSorte.addEventListener( 'click', () => {
  const max = 893;
  const min = 1;
  let tentativas = 0;
  let idDaSorte;
  let resultado;
  let naoExiste = false;
  do {
    idDaSorte = Math.floor( Math.random() * ( max - min + 1 ) + min );
    resultado = pokemonAleatorios.find( element => element === idDaSorte );
    naoExiste = resultado === undefined;
    tentativas += 1;
  } while ( naoExiste === false && tentativas <= max );
  if ( naoExiste ) {
    pokemonAleatorios.push( idDaSorte );
    pokeApi
      .buscarEspecifico( idDaSorte )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );
        renderizar( poke );
      } );
    ocultarInfo( 'nao' );
  } else {
    alert( 'Todos os pokemons foram sorteados!' );
  }
} );
