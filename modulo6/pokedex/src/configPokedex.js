let visivel = true;
const abrirPokedex = () => {
  const bt = document.querySelector( '.bt-abrir-fechar' );
  if ( visivel ) {
    document.getElementById( 'pokedexPrincipal' ).style.display = 'none';
    document.getElementById( 'parteDoisPokedex' ).style.display = 'none';
    bt.innerText = 'Abrir';
    /*eslint-disable */
    ocultarInfo( 'sim' );
  } else {
    document.getElementById( 'pokedexPrincipal' ).style.display = 'block';
    document.getElementById( 'parteDoisPokedex' ).style.display = 'block';
    bt.innerText = 'Fechar';
  } visivel = !visivel;
}

document.addEventListener( 'DOMContentLoaded', abrirPokedex );

const botao = document.querySelector( '.bt-cabecalho' );

botao.addEventListener( 'click', abrirPokedex );
