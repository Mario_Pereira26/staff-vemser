import axios from 'axios';

const url = 'http://localhost:9000/api/';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) );
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) );


export default class EnjoeiApi {
/*---------------Buscar todos---------------*/
  buscarBanners() {
    return _get( `${ url }banners` );
  }

  buscarProdutos() {
    return _get( `${ url }produtos` );
  }

  buscarProdutosPorCategoria( idCategoria ) {
     return _get( `${ url }produtos?idCategoria=${idCategoria}` );
  }

  buscarTodosDetalhes() {
    return _get( `${ url }detalhes` );
  }

  buscarTodasCategorias() {
    return _get( `${ url }categorias` );
  }

  buscarVendedores() {
    return _get( `${ url }vendedor` );
  }

  async buscarTodosComentariosPorProduto( id ) {
    return await _get( `${ url }comentarios?idProduto=${ id }`);
  }
/*---------------Fim todos---------------*/
/*---------------Buscar Um---------------*/
  async buscarProduto( id ) {
    const response = await _get( `${ url }produtos?id=${ id }`);
    return response[ 0 ];
  }

  async buscarDetalhePorIdProduto( id ) {
    const response = await _get( `${ url }detalhes/?idProduto=${ id }`);
    return response[ 0 ];
  }

  async buscarUserPorEmail( email ) {
    const response = await _get( `${ url }users/?email=${ email }`);
    return response[ 0 ];
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ url }detalhes/${ id }`);
    return response[ 0 ];
  }

  async buscarCategoria( id ) {
    const response = await _get( `${ url }categorias/${ id }`);
    return response[ 0 ];
  }

  async buscarVendedor( id ) {
    return await _get( `${ url }vendedor/${ id }`);
  }
/*-------------Fim BuscarUm--------------*/
/*-------------registar Um---------------*/
  async registrarComentario( idProduto, mensagem ) {
    const response = await _post( `${ url }comentarios`, { mensagem, idProduto } );
    return response;
  }

  async registrarUser( { nome, email, senha } ) {
    const response = await _post( `${ url }users`, { nome, email, senha } );
    return response;
  }
/*-------------registar Um--------------*/
}