import Produtos from "./produtos";

export default class ListaProdutos {

	constructor( ListaProdutosDoServidor = [] ){
		this._todos = ListaProdutosDoServidor.map( elem => new Produtos( elem.id, elem.idVendedor, elem.idCategoria, elem.nome, elem.preco, elem.imagem  ) );
  }

  adicionarDetalhes( detalhe, id ) {
    this._todos.find( prod => {
      if( prod === id ) { prod.detalhes = detalhe }
    } )
  }
  
  get todos() {
    return this._todos;
  }

  buscarProdutosPorCategoria( idCategoria ) {
     return this._todos.filter( produto => produto.idCategoria === idCategoria );
  }

}