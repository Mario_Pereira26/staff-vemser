import EnjoeiApi from "./enjoeiApi";

export default class Produtos {
  constructor( id, idVendedor, idCategoria, nome, preco, imagem ) {
    this.enjoeiApi = new EnjoeiApi();
    this.id = id;
    this.idVendedor = idVendedor;
    this.idCategoria = idCategoria;
    this.nome = nome;
    this.preco = preco;
    this.imagem = imagem;
  }
}
