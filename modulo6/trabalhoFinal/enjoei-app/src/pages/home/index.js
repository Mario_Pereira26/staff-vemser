import React, { Component } from 'react';
import { HeaderUi, ColunaLeft, ColunaCenter, ColunaRight, CorpoBotton, MenuNav, FooterDescricoes, FooterUi } from '../../components';
import { Row } from 'antd';
import { EnjoeiApi } from '../../models';
import '../css/geral.css';
export default class Home extends Component {
  
  constructor( props ) {
    super( props );
    this.mensagem = "frete grátis na primeira compra - promo válida no frete padrão até R$ 20";
    this.enjoeiApi = new EnjoeiApi();
    this.id = props.location.state.id || 1;
    this.idCategoria = props.location.state.idCategoria;
  }

  render(){
    const { mensagem, id, idCategoria } = this;
    return ( 
      <React.Fragment>
        <div className="container">
          <HeaderUi dash="true" mensagem={mensagem} />
          <MenuNav />
          <section className="content">
            <div className="container">
              <Row>
                <ColunaLeft id={ id } />
                <ColunaCenter />
                <ColunaRight id={ id } />
                <CorpoBotton id={ idCategoria ? idCategoria : id }/>
              </Row>
              <FooterDescricoes />
            </div>
          </section>
          <FooterUi interno={ true } />
        </div>
      </React.Fragment>
    )
  };
}
