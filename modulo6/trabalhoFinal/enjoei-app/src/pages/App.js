import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from '../components/rotaPrivada';
import Login from './login';
import Dashboard from './dashboard';
import Home from './home';
import './css/bordas.css';
import './css/botoes.css';
import './css/colunas.css';
import './css/containers.css';
import './css/footer.css';
import './css/grid.css';
import './css/imagens.css';

import Formulario from '../components/cadastro';

export default class App extends Component {
  render(){
    return(
      <div className="App">
        <Router>
          <Route path="/login" exact component={ Login } />
          <Route path="/formulario" exact component={ Formulario } />
          <Route path="/" exact component={ Dashboard } />
          <PrivateRoute path="/home" exact component={ Home } />
        </Router>
      </div>
    );
  }
}
