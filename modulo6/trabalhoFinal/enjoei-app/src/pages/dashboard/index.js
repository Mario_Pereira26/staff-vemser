import React, { Component } from 'react';
import { HeaderUi, MenuNav, BannerUi, Postagem, FooterUi } from '../../components';
import { EnjoeiApi } from '../../models';
import { Row, Col } from 'antd';
export default class Dashboard extends Component {
  
  constructor( props ) {
    super( props );
    this.enjoeiApi = new EnjoeiApi();
    this.state = { postagem : [] };
  }

  componentDidMount() {
    this.enjoeiApi.buscarTodasCategorias().then( resposta => {
      this.setState( state => { return { ...state, postagem: resposta } } );
    } );
  }

  render(){
    const { postagem } = this.state;
    return ( 
      <React.Fragment>
        <div className="container">
          <HeaderUi dash="true" />
          <Row>
            <Col span={24}>
              <MenuNav />
            </Col>
            <Col span={24}>
              <BannerUi />
            </Col>
          </Row>
          { postagem &&
            postagem.map( p => <Postagem key={`post${p.id}`} ordem={ p.id } idCategoria={ p.idCategoria } titulo={ p.nome } descricao={ p.subTitulo } botao={ 'mais' }/> )
          }
          <FooterUi />
        </div>
      </React.Fragment>
    )
  };
}
