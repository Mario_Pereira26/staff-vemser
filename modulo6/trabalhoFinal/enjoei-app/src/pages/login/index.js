import React, { Component } from 'react';

import LoginUi  from '../../components/loginUi';
import './login.css';
import { HeaderUi, FooterUi } from '../../components';
import { Row, Col } from 'antd';

export default class Login extends Component {
  constructor( props ){
    super(props);
  }
  render(){
    
    return (
      <>
        <HeaderUi />
        <div className="container">
          <Row>
            <Col xs={ { span: 24 } }>
              <div className="login">
                <div className="layout-login">
                  <LoginUi />
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <FooterUi />
      </>
    )
  };
}