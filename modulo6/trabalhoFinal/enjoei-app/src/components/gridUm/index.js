import React, { useEffect, useState } from 'react';
import { Row, Col, Image, Card } from 'antd';
import { Link } from 'react-router-dom';

function GridUm( props ) {
  const { Meta } = Card;
  const [ nome, setNome  ] = useState( props.nome );
  const [ idVendedor, setIdVendedor ] = useState( props.idVendedor );
  const [ preco, setPreco ] = useState( props.preco );
  const [ imagem, setImagem ] = useState( props.imagem );
  const [ id, setId ] = useState( props.id );
  const [ idCategoria, setIdCategoria ] = useState( props.idCategoria );

  return (
    <>
    <Row>
      <Col ms={{span: 24}} md={{span: 22}}>
        < Link to={ { pathname: "/home", state: {id: id, idCategoria: idCategoria, idVendedor: idVendedor} } } >
          <Image
            src="error"
            fallback={imagem}
          />
          <span className='texto-descritivo'>{preco}</span>
        </Link>
        
      </Col>
    </Row>
    </>
  );
}

export default GridUm;
