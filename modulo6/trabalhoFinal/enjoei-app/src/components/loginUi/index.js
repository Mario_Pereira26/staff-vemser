import React, { useEffect, useState } from 'react';
import { Button, Row, Col } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { Divider, Checkbox } from 'antd';
import EnjoeiApi from '../../models/enjoeiApi';

function LoginUi( ) {
  const history = useHistory();
  const [ login, setLogin ] = useState();
  const [ senha, setSenha ] = useState();
  const [ logar, setLogar ] = useState();

  function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
  }

  useEffect( () => {
    if( login !== undefined && senha !== undefined ) {
      const enjoeiApi = new EnjoeiApi();
      enjoeiApi.buscarUserPorEmail( login ).then( respostas => {
          if( respostas !== undefined){
            if( respostas.senha === senha){
              localStorage.setItem('user', respostas.id)
              history.push('/')
            }
          }
          
        })
    }
  },[logar] );

  return (
    <>
      <section className="container">
        <h2>faça login no enjoei</h2>
        <div className="teste">
          <Button className="btn btFace" >Entre usando o facebook</Button>
          <Divider plain>ou</Divider>
          <label htmlFor="email">e-mail</label>
          <input onBlur={ evt => setLogin( evt.target.value )} id="email" type="text" name="email"/>
          <label htmlFor="senha">senha super secreta</label>
          <input onBlur={ evt => setSenha( evt.target.value )} type="password" id="senha" name="senha"/>
          <Col xs={ { span: 24 } }>
            <Row>
              <Col xs={ { span: 12 } }>
                <Checkbox className="checkBox" onChange={onChange}>continuar conectado</Checkbox>
              </Col>
              <Col xs={ { span: 12 } }>
                <Link to='/#' className="escrita-rosa lado-right">esqueci a senha</Link>
              </Col>
            </Row>
          </Col>
          <Button className="btn fundo-rosa" onClick={ evt => setLogar( evt.target.value++ )} >Entrar</Button>
        </div>
        <Link className="texto-centralizado escrita-rosa" to='/formulario' >não tenho conta</Link>
      </section>
    </>
  )
}

export default LoginUi;
