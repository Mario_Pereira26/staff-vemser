import React, { useEffect, useState } from 'react';
import { EnjoeiApi } from '../../models';
import { Col, Image, Divider } from 'antd';
import { Chat } from '../';

function ColunaLeft( props ) {

  const [ produto, setProduto ] = useState([]);
  const [ id, setId ] = useState( props.id );

  useEffect(() => {
    const enjoeiApi = new EnjoeiApi();
    enjoeiApi.buscarProduto( id ).then( resposta => setProduto( resposta ) );
  }, [] );

  return (
    <>
      <Col ms={ { span: 16 } } md={ { span: 16 } } lg={ { span: 10 } } className="coluna">
        <Col span={24}>
          <Image className="imagem-home"
            src="error"
            fallback={produto.imagem}
          />
        </Col>
        <Col span={24}>
            <div className="container-miniatura">
              <span>
                <Image className="imagem-miniatura"
                  src="error"
                  fallback={produto.imagem}
                />
              </span>
            </div>
        </Col>
        <Divider />
        <Chat id={id} />
      </Col>
    </>
  )
}

export default ColunaLeft;
