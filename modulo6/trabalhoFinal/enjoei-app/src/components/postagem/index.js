import React, { useEffect, useState } from 'react';
import GridUm from '../gridUm';
import { EnjoeiApi } from '../../models';
import { Row, Col } from 'antd';
import './postagem.css';
import { RightOutlined } from '@ant-design/icons';

function Postagem( props ) {
  const { ordem, titulo, botao, descricao } = props;
  const [ produtos, setProdutos ] = useState([]);

  useEffect(() => {
    const enjoeiApi = new EnjoeiApi();
    enjoeiApi.buscarProdutosPorCategoria( ordem ).then( resposta => setProdutos( resposta ) );
  }, [] );

  const formatDinheiro = (preco) => {
    return preco.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
  }

  function colectionQuatroFotos ( flag, fotos ) {
    return fotos.map( (item, i) => {
      if( flag ? ( i < 4 ) : ( i >= 0 && i < 4 ) ) {
        return  <Col key={`colection${i}${item.id}`} sm={{span: 24}} md={{span: 11}} className="espacamento">
                  <GridUm id={ item.id } nome={ item.nome } preco={ formatDinheiro( item.preco ) } imagem={ item.imagem } />
                </Col>
      }
    } )
  };

  function colectionColunaUmFoto ( posicao ) {
    return (
      <Col sm={{span: 24}} md={{span: 16}} lg={{span: 8}}>
        <div className="img-post">
          <GridUm  id={ produtos[ posicao ].id } idCategoria={ produtos[ posicao ].idCategoria } nome={produtos[ posicao ].nome} preco={ formatDinheiro( produtos[ posicao ].preco ) } imagem={produtos[ posicao ].imagem} /> 
        </div>
        
      </Col> )
  }

  function ordemApresentacao () {
    const flag = ordem%2 === 0 ? true : false;
    return ( 
      <>
        <Row>
          { flag && colectionColunaUmFoto(0) }
          { flag && colectionColunaUmFoto(1) }
          <Col sm={{span: 24}} lg={{span: 8}}>
            <Row >
                { colectionQuatroFotos( flag, produtos ) }
            </Row>
          </Col>
          { flag === false && colectionColunaUmFoto(4) }
          { flag === false && colectionColunaUmFoto(5) }
        </Row>
      </> )
  }

  return (
    !produtos.length > 0 ? (
      <h3> Aguarde... </h3>
    ) :
    <> 
      <section className="content" key={`postagem${ordem}`}>
        <div className="container">
          <Row>
            <Col span={22} className="ajustes-banner">
              <h3>{titulo}</h3>
              <span>{ descricao }</span>
              <div className="posicao-right">   
                <a href="/#">{botao} <RightOutlined /></a>
              </div>
            </Col>
            <Col span={22} className="ajustes-banner">
              {ordemApresentacao()}
            </Col>
          </Row>
        </div>
      </section>
    </>
  )
}

export default Postagem;
