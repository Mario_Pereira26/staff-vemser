import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import { GridCinco } from '../';
import { EnjoeiApi } from '../../models';
import './stilo.css'
function Ofertas( props ) {

  const [ texto, setTexto ] = useState( props.texto );
  const [ desconto, setDesconto ] = useState( props.desconto );
  const [ complemento, setComplemento ] = useState( props.complemento );
  const [ produtos, setProdutos ] = useState( [] );

  useEffect(() => {
    const enjoeiApi = new EnjoeiApi();
    enjoeiApi.buscarProdutosPorCategoria( props.id ).then( resposta => setProdutos( resposta ) );
  }, [props.id] );

  return (
    <>
      <Col span={24}>
        <div className="titulo-lista espaco-top espaco-bottom">
          <p className="espaco-left font-bold">{texto} <span className='cor-rosa'>{desconto} </span>{complemento}</p>
          <span className="alinhar-right">ver mais</span>
        </div>
      </Col>
      <Row>
        <GridCinco produtos={ produtos } />
      </Row>
      
    </>
  );
}

export default Ofertas;
