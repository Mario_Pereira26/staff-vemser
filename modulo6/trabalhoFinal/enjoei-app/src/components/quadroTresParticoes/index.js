import React, { useEffect, useState } from 'react';
import { EnjoeiApi } from '../../models';
import { Row, Col, Avatar, Image, Divider } from 'antd';
import { Estrelas } from '../';

function TresParticoes( props ) {
  const { idVendedor } = props;
  const [ vendedor, setVendedor ] = useState();
  const [ data, setData ] = useState();
  
  useEffect(() => {
    const enjoeiApi = new EnjoeiApi();
    
    enjoeiApi.buscarVendedor( idVendedor ).then( resposta => {
      setVendedor( resposta );
      let dataAuxiliar = new Date(resposta.criacao);
      var month = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"][dataAuxiliar.getMonth()];
      setData( `${ month }/${ dataAuxiliar.getFullYear() }` )
    } );
  }, [idVendedor] );
  
  return (
    <>
      {vendedor && 
      <Col span={24} className="coluna margin-top borda">
        <Row className="texto-center">
          <Col span={24}>
            <Row className="borda-botton">
              <Col span={12}>
                <div className="container-avatar">
                  <Avatar size={64} className="avatar avatar-conf"
                    src={<Image src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  />
                  <div>
                    <ul className="container-dados-vendedor ajuste-lista">
                      <li>{ vendedor.nome }</li>
                      <li className="font-menor-clara">{ `${vendedor.cidade}/${vendedor.estado}`}</li>
                    </ul>
                  </div>
                </div>
              </Col>
              <Col span={12}>
                <button className="botao-seguir cor-rosa">seguir</button>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row className="borda-botton">
              <Col  ms={ { span:24 } } md={ { span: 7 } } className="centralizar" >
                <p className="font-menor-clara margin-top">Avaliação</p>
                <span className="estrelas-conf"><Estrelas  avaliacao={5} permissao={true}/></span>
              </Col>
              <Divider type="vertical" />
              <Col  ms={ { span:24 } } md={ { span: 7 } } className="centralizar">
                <p className="font-menor-clara margin-top texto-center ">últimas entregas</p>
                <span className="estrelas-conf"><Estrelas avaliacao={5} permissao={true}/></span>
              </Col>
              <Divider type="vertical" />
              <Col  ms={ { span:24 } } md={ { span: 7 } } className="centralizar">
                <p className="font-menor-clara margin-top">tempo médio de envio</p>
                <p className="font-bold">{ vendedor.tempoMedio }</p>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row className="borda-botton">
              <Col span={8}>
                <p className="font-menor-clara margin-top">à venda</p>
                <p className="font-bold">41</p>
              </Col>
              <Col span={8}>
                <p className="font-menor-clara margin-top">últimas entregas</p>
                <p className="font-bold">303</p>
              </Col>
              <Col span={8}>
                <p className="font-menor-clara margin-top">no enjoei desde</p>
                <p className="font-bold">{ data }</p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>}
    </>
  );
}

export default TresParticoes;
