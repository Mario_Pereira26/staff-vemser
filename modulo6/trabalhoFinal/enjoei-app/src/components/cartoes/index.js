import React, { useEffect, useState } from 'react';

import { Row, Col,Image } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';
import svgVisa from '../svgs/svgVisa.svg';
import SvgMaster from '../svgs/svgMaster.svg';
import SvgBarras from '../svgs/svgBarras.svg';

function Cartoes( props ) {

  return (
    <>
      <Row>
        <Col span={2} className="tamanho">
          <span >
            <Image src={ svgVisa } />
          </span>
        </Col>
        <Col span={2} className="tamanho">
          <span >
            <Image src={ SvgMaster } />
          </span>
        </Col>
        <Col span={2} className="tamanho">
          <span>
            <Image src={ SvgBarras } />
          </span>
        </Col>
        <Col span={2} className="espacamento-colunas">
          <InfoCircleOutlined />
        </Col>
      </Row>
    </>
  )
}

export default Cartoes;
