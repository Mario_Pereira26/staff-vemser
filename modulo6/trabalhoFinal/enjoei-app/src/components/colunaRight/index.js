import React, { useEffect, useState } from 'react';
import { EnjoeiApi } from '../../models';
import { Link } from 'react-router-dom';
import { Col, Divider } from 'antd';
import { TresParticoes, QuatroParticoes, Cartoes } from '../';


function ColunaRight( props ) {
  const { id } = props;
  const [ produto, setProduto ] = useState( [] );
  const [ detalhe, setDetalhe ] = useState( [] );
  
  useEffect(() => {
    const enjoeiApi = new EnjoeiApi();
    const requisicoes = [
      enjoeiApi.buscarProduto( id ),
      enjoeiApi.buscarDetalhePorIdProduto( id )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        setProduto( respostas[0] );
        setDetalhe( respostas[1] );
      })
  }, [] );

  return (
    <>
      <Col ms={{span: 24}} lg={ { span: 10 } } className="coluna">
        <div className="container-detalhes">
          <span>moças / acessórios / cintos</span>
          <h2>{ produto.nome }</h2>
          <h4>{ detalhe.marca } <Divider type="vertical" /> <Link className="cor-rosa" to="/#">seguir marca</Link> </h4>
          <h2 className="cor-rosa">R$ { produto.preco - 20 } <span className="tachar">R${produto.preco }</span> <span className="etiqueta"> R$ -20 na 1° compra</span></h2>
          <h4>10 x R$ { ( produto.preco - 20 ) / 10 } sem juros</h4>
          
          <Col span={20} className="coluna cartoes">
            <Cartoes />
          </Col>
          
          <button className="botao-padrao fundo-rosa borda">eu quero</button>
          <button className="botao-padrao fundo-branco borda">adicionar à sacolinha</button>
          <button className="botao-padrao fundo-branco borda">fazer oferta</button>

          <QuatroParticoes detalhes={ detalhe } />

          <h3 className="margin-top">descrição</h3>
          <p className="margin-top">{ detalhe.descricao }</p>
          <TresParticoes idVendedor={ produto.idVendedor } />

          <div className="margin-top">
            <li>comprar no enjoei é seguro</li>
            <li>se não gostar, você pode devolver. entenda</li>
          </div>
        </div>
        
      </Col>
    </>
  )
}

export default ColunaRight;
