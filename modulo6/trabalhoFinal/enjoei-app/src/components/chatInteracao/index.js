import React, { useEffect, useState } from 'react';
import { EnjoeiApi } from '../../models';
import { Row, Col, Input } from 'antd';
import { Perguntas } from '../';

function Chat( props ) {
  
  const { id } = props;
  const [ comentarios, setComentarios ] = useState([]);
  const [ comentario, setComentario ] = useState('');
  const [ enviarAcao, setEnviar ] = useState(0);

  const enviar = () => {
    const enjoeiApi = new EnjoeiApi();
    enjoeiApi.registrarComentario( id, comentario ).then();
    setEnviar( enviarAcao + 1 );
  };

  useEffect( () => {
    const enjoeiApi = new EnjoeiApi();
    enjoeiApi.buscarTodosComentariosPorProduto( id ).then( resposta => setComentarios( resposta ));
  }, [enviarAcao] );

  return (
    <>
      <Row>
        <div className='container-pergunta'>
          <Col ms={{span: 24}} md={ { span: 18 } } >
            <Input
              className="altura-imput"
              placeholder="pergunte ao vendendor"
              onBlur={ evt => setComentario( evt.target.value ) }
            />
          </Col>
          <Col ms={{span: 24}} md={ { span: 4 } } >
            <span className="bt-pergunta">
              <button className='botao-seguir bt-pergunta cor-rosa altura-imput' onClick={enviar}>perguntar</button>
            </span>
          </Col>
        </div>
      </Row>
      <Row className="espaco-top">
        <Col span={24}>
          <div className="padding-laterais">
            <Col span={24}>
              <h3 >Últimas perguntas</h3>
            </Col>
            <div className="container-interacoes">
              {
                comentarios && comentarios.map( e => {
                  return (
                    <>
                      <Perguntas key={`pergunta${e.id}`} pergunta={ e.mensagem } user={ 'mirian ' } time={ 'há 8 horas' } />
                      <Perguntas key={`resposta${e.id}`} pergunta={ 'olá!, em breve responderemos!' } resposta={ true } user={ '' } time={ '' } />
                    </>
                  );
                } )
              }
            </div>
          </div>
        </Col>
      </Row> 
    </>
  )
}

export default Chat;
