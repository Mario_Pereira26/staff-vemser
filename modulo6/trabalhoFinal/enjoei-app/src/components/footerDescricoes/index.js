import React from 'react';
import { Row } from 'antd';
import { ColunaSimples, ColunaDubla } from '../';
import './stilo.css';
const FooterDescricoes = () => 
  <footer className="footer"> 
    <Row>

      <ColunaSimples titulo={'utilidades'} opcoes={
        [
          'ajuda',
          'como vender',
          'como comprar',
          'marcas',
          'termos de uso',
          'política de privacidade',
          'trabalhei no enjoei',
          'black friday',
          'investidores'
        ] }
      />

      <ColunaSimples titulo={'minha conta'} opcoes={
        [
          'minha loja',
          'minhas vendas',
          'minhas compras',
          'enjubank',
          'yeyezados',
          'configurações'
        ] }
      />

      <ColunaDubla titulo={'marcas populares'}
        colunaUm={
          [
            'farm',
            'melissa',
            'forever 21',
            'nike',
            'adidas',
            'kipling',
            'ver todas'
          ]
        }
        colunaDois={
          [
            'zara',
            'arezzo',
            'schutz',
            'tommy hilfiger',
            'antix',
            'apple'
          ]
        }
      />
      <ColunaSimples titulo={'siga a gente'} opcoes={
        [
          'facebook',
          'twitter',
          'instagram'
        ] }
      />

    </Row>
  </footer>

export default FooterDescricoes;
