import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';

export default function ColunaDubla( { colunaUm, colunaDois, titulo } ) {

  return (
    <>
      <Col  ms={{span:24}} md={ { span: 6 } } className='conf-coluna'>
          <ul>
            <h3>{titulo}</h3>
            <Row>
              <Col key={`link1`} span={12}>
                {
                  colunaUm.map( (link,i) => <li key={`linkc1${i}`}><Link to="/#">{link}</Link></li> )
                }
              </Col>
              <Col span={8}>
                {
                  colunaDois.map( (link,i) => <li key={`linkc2${i}`}><Link to="/#">{link}</Link></li> )
                }
              </Col>
            </Row>
            
          </ul>
          
      </Col>
    </>
  );
}