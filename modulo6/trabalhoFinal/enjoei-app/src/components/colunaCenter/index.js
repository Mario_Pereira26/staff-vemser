import React, { useEffect, useState } from 'react';

import { Col } from 'antd';

function ColunaCenter( props ) {

  return (
    <>
      <Col md={{span: 24}} lg={ { span: 2 } } className="coluna-meio">
        <div className='icons-centrais'>
          <h3 className="botao-mao"></h3>
          <h3 className="botao-msg"></h3>
        </div>
      </Col>
    </>
  )
}

export default ColunaCenter;
