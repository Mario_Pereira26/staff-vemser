import React, { useState, useEffect } from 'react';
import { HeaderUi, FooterUi } from '../../components';
import { Link, useHistory } from 'react-router-dom';
import { Button } from 'antd';
import Enjoei from '../../models/enjoeiApi';

function Formulario() {
  const history = useHistory();

  const [ nome, setNome ] = useState();
  const [ email, setEmail ] = useState();
  const [ senha, setSenha ] = useState();
  const [ cadastrar, setCadastrar ] = useState();
  const [ mensagem, setMensagem ] = useState('');

  useEffect( () => {
    if( nome !== undefined && email !== undefined && senha !== undefined ) {
      const enjoeiApi = new Enjoei();
      const requisicoes = [
        enjoeiApi.buscarUserPorEmail( email)
      ];
  
      Promise.all( requisicoes )
        .then( respostas => {
          if( respostas[0] === undefined){
            enjoeiApi.registrarUser( { nome:nome, email:email, senha:senha } ).then( resposta =>
              localStorage.setItem("user",resposta.id)
            )
            setMensagem("Cadastrado!!");
            history.push('/')
          } else {
            setMensagem("Usuário ja existe!!");
          }
          
        })
    }
  },[cadastrar] );

  return (
    <React.Fragment>
      <div className="container">
      <HeaderUi />
        <div className="row">
          <div className="col col-sm-12">
            <div className="login">
              <div className="layout-login">
                <h2>Cadastro Enjoei:</h2>
                <input type="text" placeholder="Digite seu nome completo" onBlur={ evt => setNome( evt.target.value ) } required />
                <input type="text" placeholder="Digite seu email" onBlur={ evt => setEmail( evt.target.value ) } required />
                <input type="text" placeholder="Senha desejada" onBlur={ evt => setSenha( evt.target.value ) } required />
                <Button onClick={ evt => setCadastrar( evt.target.value++ )} className="btn btFace" >Cadastrar</Button>
                <Link to='/login' className="escrita-rosa lado-right" ><button className="spaco-button">Voltar</button></Link>
                <p>{ mensagem }</p>
              </div>
            </div>
          </div>
        </div>
        <FooterUi />
      </div>
    </React.Fragment>
  )
}

export default Formulario;