import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';

function QuatroParticoes( props ) {

  const { marca, condicao, codigo } = props.detalhes;
  const [ tamanho, setTamanho ] = useState( '-' );

  return (
    <>
      <Col span={24} className="coluna borda">
        <Row className="texto-center">
          <Col span={24} className="titulo-tam">
            <span>tamanho</span>
            <p>{tamanho}</p>
          </Col>
          <Col span={8} className="borda-right">
            <p className="margin-top">marca</p>
            <p>{ marca }</p>
          </Col>
          <Col span={8} className="borda-right">
            <p className="margin-top">condição</p>
            <p>{ condicao }</p>
          </Col>
          <Col span={8}>
            <p className="margin-top">código</p>
            <p>{ codigo }</p>
          </Col>
        </Row>
      </Col>
    </>
  );
}

export default QuatroParticoes;
