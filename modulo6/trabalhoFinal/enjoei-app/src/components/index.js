import HeaderUi from './headerUi';
import MenuNav from './menuNavegacao';
import LoginUi from './loginUi';
import FooterUi from './footerUi';
import BannerUi from './bannerUi';
import Postagem from './postagem';
import Estrelas from './estrelasUi';
import Perguntas from './perguntaUi';
import GridUm from './gridUm';
import GridCinco from './gridCinco';
import Ofertas from './ofertas';
import FooterDescricoes from './footerDescricoes';
import ColunaSimples from './colunaSimples';
import ColunaDubla from './colunaDupla';
import TresParticoes from './quadroTresParticoes';
import QuatroParticoes from './quadroQuatroParticoes';
import Chat from './chatInteracao';
import ColunaLeft from './colunaLeft';
import ColunaCenter from './colunaCenter';
import ColunaRight from './colunaRight';
import CorpoBotton from './corpoBotton';
import Cartoes from './cartoes';

export {
  HeaderUi,
  MenuNav,
  LoginUi,
  FooterUi,
  BannerUi,
  Postagem,
  Estrelas,
  Perguntas,
  GridUm,
  GridCinco,
  Ofertas,
  FooterDescricoes,
  ColunaSimples,
  ColunaDubla,
  TresParticoes,
  QuatroParticoes,
  Chat,
  ColunaLeft,
  ColunaCenter,
  ColunaRight,
  CorpoBotton,
  Cartoes
};