import React from "react";
import './header.css';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';
import ArrowBackOutlinedIcon from '@material-ui/icons/ArrowBackOutlined';

export default function HeaderUi (props) {
  const { dash, mensagem } = props;
  return (
    <React.Fragment>
      <header className={`header ${dash ? 'flash-mensagem':''}`}>

          <Row>
            <Col span={24}>
              {
                dash ? mensagem ? <p>🚚 {mensagem}</p> : <p>
                    <span>voa, frete, voa 🕊️ só R$ 3,99</span> - por tempo limitado nesse <u>baando de produtos</u></p> :
                <>
                  <Col span={1}>
                    <Link to="/"><ArrowBackOutlinedIcon /></Link>
                  </Col>
                  <Col span={22} >
                    <h1 className="logo">
                      <Link to="/" >Home enjoei</Link>
                    </h1>
                  </Col>
                </>
              }
            </Col>
          </Row>

      </header>
    </React.Fragment>
  );
}
