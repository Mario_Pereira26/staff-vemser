import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col , Image} from 'antd';
import iphone from './iphone.svg';
import './stilo.css';
const FooterUi = ( { interno }) => 
  <footer className="footer"> 

    { interno ? 
      <Row>
        <Col  ms={ { span: 5 } } className='centralizar'>
          <h1 className="logo-footer">
            <Link to="/" >Home enjoei</Link>
          </h1>
        </Col>
        <Col  ms={ { span:20 } } md={ { span: 16 } } className="alinhar-left">
          <p>enjoei &copy; 2020 - todos os direitos reservados - enjoei.com.br atividades de internet S.A. CNPJ: 16.922.038/00001-51</p>
          <p>av. isaltino victor de moraes, 437, vila bonfim, embu das artes, sp, 06806-400 - (11) 3197-4883</p>
        </Col>
        <Col ms={ { span:24 } } md={ { span: 4 } } className='centralizar' >
          <Link to="/#">
            <div className="botao">
              <div className="icon-app">
                <img src={iphone}  />
              </div>
              <span>baixe o enjuapp</span>
            </div> 
          </Link>
        </Col>
      </Row>
      :
      <p>
        Protegido por reCAPTCHA - <a className="cor-rosa" href='/#'>privacidade</a> - <a className="cor-rosa" href='/#'>Condições</a>
      </p> 
    }
    
  </footer>

export default FooterUi;
