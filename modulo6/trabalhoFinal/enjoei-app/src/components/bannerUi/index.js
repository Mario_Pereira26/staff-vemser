import React, { useEffect, useState } from 'react';
import { EnjoeiApi } from '../../models';
import { Row, Col, Carousel, Image } from 'antd';
import './banner.css';

function BannerUi() { 

  const [ slide, setSlide ] = useState([]);

  useEffect( () => {
    const enjoeiApi = new EnjoeiApi();

    enjoeiApi.buscarBanners().then( resposta => setSlide( resposta ) );
    
  },[] );
  
  return (
    <>
        <Row>
          <Col  xs={{span: 24}} md={{span: 22}} >
            <div className="ajustes-banner">
              <Carousel className="margin-left" autoplay>
                  { slide &&
                    slide.map( e => {
                      return (
                        <div key={`banner${e.id}`}>
                          <img className="img-banner" src={e.imagem}  /> 
                        </div>
                      )
                    })
                  }
              </Carousel>
            </div>
          </Col>
        </Row>
     
    </>
  )
}

export default BannerUi;
