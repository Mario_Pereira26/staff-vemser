import React, { useEffect, useState } from 'react';

import { Row, Divider } from 'antd';
import { Ofertas } from '../';

function CorpoBotton( props ) {
  const id = props.id;
  const ofertas = [
    { 'texto': 'essa loja oferece até ', 'desconto': '30% de desconto + frete único', 'complemento': 'nas compras de sacolinha'},
    { 'texto': 'você também vai curtir'}
  ]

  return (
    <>
      <Row>
        {
          ofertas.map( (e,i) => <Ofertas key={`ofertaKey${i}`} id={ id } texto={e.texto} desconto={e.desconto} complemento={e.complemento}/> )
        }
        <Divider />
      </Row>
    </>
  )
}

export default CorpoBotton;
