import React from "react";
import './navegacao.css';
import { Row, Col, Input, Divider, Button, Image } from 'antd';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import svgAjuda from '../svgs/svgAjuda.svg';

const { Search } = Input;
const onSearch = value => console.log(value);

export default function MenuNav (props) {
  const history = useHistory();
    const { apiEp, atualizaHome } = props;

    const logaout = () => {
      localStorage.clear();
      history.push('/login')
    };

    return (
      <React.Fragment>
        <section className="header-nav">
          <Row>
          <header className="header">
            <div className="container">
                <Row>
                  <Col span={24}>
                    <h1 className="logo-nav">
                      <Link to="/" onClick={atualizaHome} >dashboard</Link>
                    </h1>

                    <label htmlFor="menu-check" className="menu-button">Abrir menu</label>
                    <input id="menu-check" type="checkbox"/>

                    <nav className="nav">
                      <ul>
                        <li><Search className="ajuste-seach"  placeholder={`busque "laco"`} onSearch={ apiEp } /></li>
                        <li><Link key={`mocas`} to="/#">moças</Link></li>
                        <li><Link key={`rapazes`} to="/#">rapazes</Link></li>
                        <li><Link key={`kids`} to="/#">kids</Link></li>
                        <li><Link key={`casa&tal`} to="/#">{`casa&tal`}</Link></li>
                        
                        <li key={`divVertical`}><Divider type="vertical" /></li>
                        <li key={`ajuda`}><span className="bt-ajuda"><Image src={ svgAjuda } /></span></li>
                        <li key={`bt-entrar`}><Link to="/login"><Button className="bt-entrar">Entrar</Button></Link></li>
                        <li key={`bt-vender`}><Link to="/"><Button className="bt-vender">Quero Vender</Button></Link></li>
                         
                      </ul>
                    </nav>
                  </Col>
                </Row>
            </div>
        </header>
          </Row>        
        </section>
      </React.Fragment>
    );
  
}