import React from 'react';
import { Col, Image, Card } from 'antd';
import { Link } from 'react-router-dom';
import './stilo.css'
function GridCinco( props ) {
  const { Meta } = Card;
  const { produtos=[] } = props;

  const formatDinheiro = (preco) => {
    return preco.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
  }
  
  return (
    <>
      {
        produtos.map( produto => {
          return (
            <Col key={`prod${produto.id}`}  ms={ { span: 24 } } lg={ { span: 3 } } className="espaco-left espacamento-minimo">
              < Link onClick={() => window.location.reload()}  to={ { pathname: "/home", state: {id: produto.id, idCategoria: produto.idCategoria} } } >
                <div className='container-card'>
                  <Card  hoverable >
                    <Image className="img"
                      src="error"
                      fallback={ produto.imagem }
                    />  
                  </Card>
                  <Meta title={ produto.nome } description={ formatDinheiro( produto.preco ) }/>
                </div>
              </Link>
            </Col>
          );
        })    
      }
    </>
  );
}

export default GridCinco;
