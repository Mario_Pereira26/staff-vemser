import React, { useEffect, useState } from 'react';
import { Row, Col, Image, Avatar } from 'antd';
import './stilo.css';

function Perguntas( props ) {
  
  const [ pergunta, setPergunta ] = useState( props.pergunta );
  const [ resposta, setResposta ] = useState( props.resposta );
  const [ user, setUser] = useState( props.user );
  const [ time, setTime] = useState( props.time );

  return (
    <>
      <Col span={24}>
        <Row>
          <Col span={2} className="margin-bottom">
            <Avatar size={40} className=" avatar-conf"
              src={<Image src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
            />
          </Col>
          <Col span={22}>
            <ul className='ajuste-lista'>
              <li><span className={`interacao ${resposta ? 'resposta': 'pergunta'}`}>{pergunta }</span></li>
              <li><span className={'dados'}>{user}{time}</span></li>
            </ul>
          </Col>
        </Row>
      </Col>
    </>
  );
}

export default Perguntas;
