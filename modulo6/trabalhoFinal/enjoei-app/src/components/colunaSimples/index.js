import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Col } from 'antd';

export default function ColunaSimples( { opcoes, titulo } ) {
  
  const [links, setLinks] = useState(opcoes);

  return (
    <>
      <Col className='conf-coluna' ms={{span:24}} md={ { span: 6 } } >
          <ul>
            <h3 >{titulo}</h3>
            {
              links.map( ( link, i ) => <li key={`link${i}`}><Link to="/#">{link}</Link></li> )
            }
          </ul>
      </Col>
    </>
  );
}