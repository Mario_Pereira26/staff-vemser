import EpisodiosApi from "./episodiosAPI";

export default class Episodio {
  constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVotos, totalVotos ) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.url = thumbUrl;
    this.qtdVezesAssistido = 0;
    this.episodioApi = new EpisodiosApi();
    this.qtdVotos = qtdVotos;
    this.totalVotos = totalVotos;
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`;
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2, 0 ) } X ${ this.ordem.toString().padStart( 2, 0 ) }`;
  }

  avaliar( nota ) {
    this.nota = parseInt( nota );
    return this.episodioApi.registrarNota( { nota: this.nota, episodioId: this.id } );
  }

  marcarComoAssistido() {
    this.assistido = true;
    this.qtdVezesAssistido++;
  }
  
  validarNota( nota ) {
    return (nota >= 1 && nota <= 5);
  }
}