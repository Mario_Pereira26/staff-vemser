import axios from 'axios';

const url = 'http://localhost:9000/api/';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) );
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) );

export default class EpisodiosApi {

  buscar() {
    return _get( `${ url }episodios` );
  }

  buscarTodosDetalhes() {
    return _get(`${ url }detalhes`);
  }

  async buscarEpisodio( id ) {
    const response = await _get( `${ url }episodios?id=${ id }` );
    return response[ 0 ];
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ url }episodios/${ id }/detalhes` );
    return response[ 0 ];
  }

  buscarNota( id ) {
    return _get( `${ url }notas?episodioId=${ id }` );
  }

  buscarNotas() {
    return _get( `${ url }notas` );
  }

  buscarTodasNotas() {
    return _get( `${ url }notas` );
  }

  async registrarNota( { nota, episodioId } ) {
    const response = await _post( `${ url }notas`, { nota, episodioId } );
    return response[ 0 ];
  }

  filtrarPorTermo( termo ) {
    return _get(`${ url }detalhes?q=${ termo }`);
  }

  async adicionarUser( { nome, cidade, telefone, email, login, senha } ) {
    
    const response = await _post( `${ url }user`,{ nome, cidade, telefone, email, login, senha }  );
    return response[ 0 ];
  }

  async buscarUsuario( login, senha ) {
    const response = await _get( `${ url }user?login=${ login }&senha=${senha}` );
    return response[0].id;
  }

}