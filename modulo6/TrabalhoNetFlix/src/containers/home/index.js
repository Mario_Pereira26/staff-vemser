import React, { Component } from 'react';
import './Home.css';
import './grid.css';
import './box.css';
import './header.css';
import './content.css';

import { EpisodiosApi, ListaEpisodios } from '../../models';
import { ListaEpisodiosUi, BotaoUi, HeaderUi } from '../../components';

export default class Home extends Component {
  
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: [],
      listaTemporaria: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {

        respostas[0].map( ep => {
          let somaNota = 0;
          let qtdVotos = 0;
          respostas[2].filter( e => {
            if( e.episodioId === ep.id ) {
              somaNota += e.nota;
              qtdVotos++;
            }
          } )
          ep.totalVotos = somaNota;
          ep.qtdVotos = qtdVotos;
        } )
        this.listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            listaEpisodios: this.listaEpisodios._todos.concat([]),
            listaTemporaria: this.listaEpisodios._todos.concat([])
          }
        })
      })
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios;
    let array = [episodio];
    this.setState((state) => {
      return { ...state,listaEpisodios: array }
    })
  }

  atualizarHome = () => {
    const { listaTemporaria } = this.state
    this.setState((state) => {
      return { ...state,listaEpisodios: listaTemporaria }
    })
  }

  filtrarPorTermo = evt => {
    const termo = evt;
    this.episodiosApi.filtrarPorTermo( termo )
      .then( resultados => {
        this.setState({
          listaEpisodios: this.state.listaTemporaria.filter( e => resultados.some( r => r.episodioId === e.id ) )
        })
      } )
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  render(){
    const { listaEpisodios, listaTemporaria } = this.state;
    const {  qtdVotos, totalVotos } = this;
    
    return (  !listaEpisodios ? (
      <h3> Aguarde... </h3>
      ) : (
            <React.Fragment>
              <HeaderUi 
                atualizaHome={ this.atualizarHome.bind(this) }
                rank={ this.sortear.bind(this) }
                apiEp={ this.filtrarPorTermo.bind(this) }
              />
              <div class="content-align">
                <section class="content">
                  <div className="container">
                    <div className="row">
                      <h1 id="titulo">Black Mirror</h1>
                      <div className="container">
                        <ListaEpisodiosUi
                          listaEpisodios={ listaEpisodios }
                          suaNota={ false }
                          objNota={ totalVotos }
                          qtdAssistido={ qtdVotos }
                        />
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </React.Fragment>
          )
    )
  };
}
