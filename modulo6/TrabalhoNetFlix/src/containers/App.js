import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { PrivateRoute } from '../components/rotaPrivada';

import Home from './home';
import ListaAvaliacoes from './avaliacoes';
import DetalhesEpisodios from './detalheEpisodio';
import Ranking from './raking';
import Login from './login';
import FormularioUi from '../components/formularioUi';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <Router>
          <Route path="/login" exact component={ Login } />
          <Route path="/formulario" exact component={ FormularioUi } />
          <PrivateRoute path="/" exact component={ Home } />
          <PrivateRoute path="/avaliacoes" exact component={ ListaAvaliacoes } />
          <PrivateRoute path="/episodio/:id" exact component={ DetalhesEpisodios } />
          <PrivateRoute path="/ranking" exact component={ Ranking } />
        </Router>
      </div>
    )
  };
}

