import React, { Component } from 'react';
import { EpisodiosApi, Episodio } from '../../models';
import { EpisodioUi, HeaderUi, BotaoUi } from '../../components'


export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodioApi = new EpisodiosApi();
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      detalhes: null,
      deveExibirMensagem: false,
      deveExibirErro: false
    };
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarEpisodio( episodioId ),
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {

        this.totalVotos = 0;
        respostas[2].map( e => {
          return this.totalVotos += e.nota ? e.nota : 0
        });

        this.qtdVotos = respostas[2].length;
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl } = respostas[0];
        this.objNota1 = respostas[2].nota ? respostas[2][0].nota : 0;
        this.setState({
          episodio: new Episodio(id, nome, duracao, temporada, ordemEpisodio, thumbUrl),
          detalhes: respostas[1],
          objNota: respostas[2]
        })
      })
      
  }

  marcarComoAssistido() {
    this.qtdVotos++;
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    this.setState( state => {
      return { ...state }
    })
  }

  registrarNota( nota ) {
    const { episodio } = this.state;
    const novaNota = nota || 0;
    episodio.avaliar( novaNota ).then( () => {});
    this.setState( state => {
      return {
        ...state
      }
    });
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState( state => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState( state => {
      return { 
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  render() {
    const { episodio, detalhes} = this.state;
    const {  qtdVotos, totalVotos } = this;
    
    
    return(!episodio ? (
      
      <h3> Aguarde... </h3>
      ) : (
      <React.Fragment>
        <HeaderUi />
        <header className="App-header">
        {
          <EpisodioUi
            episodio={ episodio }
            objNota={ totalVotos }
            qtdAssistido={ qtdVotos }
            detalhes={ detalhes }
            marcarComoAssistido={ this.marcarComoAssistido.bind( this ) }
            registrarNota={ this.registrarNota.bind( this ) }
          />
        }
        </header>
      </React.Fragment>)
    )
  }
}