import React, { Component } from 'react';

import { LoginUi } from '../../components';
import './login.css';

export default class Login extends Component {
  
  render(){
    
    return (
      <div className="container AppLogin">
        <div className="row">
          <div className="col col-sm-12">
            <div className="login">
              <div className="layout-login">
                <LoginUi />
              </div>
            </div>
          </div>
        </div>
      </div>
        
    )
  };
}