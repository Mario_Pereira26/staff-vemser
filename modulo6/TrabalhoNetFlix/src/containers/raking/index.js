import React, { Component } from 'react';

import { BotaoUi, ListaEpisodiosUi, HeaderUi } from '../../components'
import { EpisodiosApi, ListaEpisodios } from '../../models';

export default class ListaAvaliacoes extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      avaliados: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        respostas[0].map( ep => {
          let somaNota = 0;
          let qtdVotos = 0;
          respostas[2].filter( e => {
            if( e.episodioId === ep.id ) {
              somaNota += e.nota;
              qtdVotos++;
            }
          } )
          ep.totalVotos = somaNota;
          ep.qtdVotos = qtdVotos;
        } )
        let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            avaliados: listaEpisodios.avaliados
          }
        })
      })
  }

  render() {
    const { avaliados } = this.state;

    return(
      <React.Fragment>
        <HeaderUi 
        />
        <header className="App-header">
          <ListaEpisodiosUi listaEpisodios={ avaliados } suaNota={true} />
        </header>
      </React.Fragment>
    )
  }

}