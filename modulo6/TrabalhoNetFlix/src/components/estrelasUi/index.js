import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';

const useStyles = makeStyles({
  root: {
    width: 200,
    display: 'flex',
    alignItems: 'center',
  },
});

export default function Estrelas( { imdb, permissao, registrarNota } ) {
  
  const [value, setValue] = React.useState(imdb);
  const [hover, setHover] = React.useState(-1);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Rating
        className="fundo-claro"
        name="size-small"
        value={value}
        precision={0.5}
        onChange={(event, newValue) => {
          setValue(newValue);
          registrarNota(newValue);
        }}
        onChangeActive={(event, newHover) => {    
          setHover(newHover);
        }}
        readOnly={permissao}
      />
    </div>
  );
}