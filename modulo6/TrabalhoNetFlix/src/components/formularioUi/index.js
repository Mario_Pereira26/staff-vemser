import React, { useState, useEffect } from 'react';
import EpisodiosApi from '../../models/episodiosAPI';
import { Link, useHistory } from 'react-router-dom';

function FormularioUi() {
  const history = useHistory();
  const [ nome, setNome ] = useState();
  const [ cidade, setCidade ] = useState();
  const [ telefone, setTelefone ] = useState();
  const [ email, setEmail ] = useState();
  const [ login, setLogin ] = useState();
  const [ senha, setSenha ] = useState();
  const [ cadastrar, setCadastrar ] = useState();

  useEffect( () => {
    if( login !== undefined && senha !== undefined ) {
      const episodiosApi = new EpisodiosApi();
      const requisicoes = [
        episodiosApi.adicionarUser( { nome: nome, cidade: cidade, telefone: telefone, email: email, login: login, senha: senha } )
      ];
  
      Promise.all( requisicoes )
        .then( respostas => {
          history.push('/login')
        })
    }
  },[cadastrar] );

  return (
    <React.Fragment>
      <div className="container AppLogin">
        <div className="row">
          <div className="col col-sm-12">
            <div className="login">
              <div className="layout-login">
                <h3>Formulário:</h3>
                <input type="text" placeholder="Digite seu nome" onBlur={ evt => setNome( evt.target.value ) } />
                <input type="text" placeholder="Digite sua cidade" onBlur={ evt => setCidade( evt.target.value ) } />
                <input type="text" placeholder="Digite seu telefone" onBlur={ evt => setTelefone( evt.target.value ) } />
                <input type="text" placeholder="Digite seu email" onBlur={ evt => setEmail( evt.target.value ) } />
                <input type="text" placeholder="Login desejado" onBlur={ evt => setLogin( evt.target.value ) } />
                <input type="text" placeholder="Senha desejada" onBlur={ evt => setSenha( evt.target.value ) } />
                <button onClick={ evt => setCadastrar( evt.target.value++ )} >Cadastrar</button>
                <Link to='/login' ><button className="spaco-button">Voltar</button></Link>
                <p>{ nome }</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default FormularioUi;