import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Modal, Button, Row } from 'antd';

import './card.css';
import Estrelas from '../estrelasUi';

export default class ListaEpisodios extends Component {

  render() {
    const { listaEpisodios, suaNota } = this.props;
    return (
      <React.Fragment>
        <Row gutter={16}>
          {
            listaEpisodios && listaEpisodios.map( e => 
              <div className='col-sm-12 col-md-4 col-lg-2' >
                <article className="box card"> 
                  <div className="container">
                    <div className="row">
                      <Link className='linkCard' to={{ pathname: `/episodio/${ e.id }`, state: { episodio: e } }}>
                        <img alt={ e.nome } src={ e.url } />
                        <h3 label="UserName">
                          <Estrelas imdb={ e.totalVotos/e.qtdVotos || '0' } permissao={true} />
                          { `Nome: ${ e.nome }` }
                        </h3>
                        <p className="data-ep">Estreia: { new Date( e.dataEstreia ).toLocaleDateString()}</p>
                        <p>Temporada X Episódio: { e.temporadaEpisodio }</p>
                        
                      </Link>
                      <div className="col col-sm-12">    
                        <p className="ajusta-conteudo">
                          Sinopse: {e.sinopse}
                        </p>
                      </div>
                    </div>
                  </div>
                    
                </article>
              </div>
            )
          }
        </Row>
      </React.Fragment>
    );
  }
}
