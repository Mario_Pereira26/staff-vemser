import React from "react";

import { Input, Tooltip, Button } from 'antd';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
const { Search } = Input;
const onSearch = value => console.log(value);


export default function HeaderUi (props) {
  const history = useHistory();
    const { apiEp, rank, atualizaHome } = props;
    const logaout = () => {
      localStorage.clear();
      history.push('/login')
    };
    return (
      <React.Fragment>
        <header className="header">
          <div className="container">
            <div className="row">
              <div className="col col-sm-12">
                <h1 class="logo">
                  <Link to="/" onClick={atualizaHome} >Home Netflix</Link>
                </h1>
                <label for="menu-check" className="menu-button">Abrir menu</label>
                <input id="menu-check" type="checkbox" />
                <nav className="nav">
                  <ul>
                    <li><Link to="/ranking">Ranking</Link></li>
                    <li><a onClick={rank}>Assistir aleatorio</a></li>
                    <li>
                      <Search  placeholder="Ex: ministro" onSearch={ apiEp } />
                    </li>
                    <li>
                      <button onClick={logaout}>logoff(sair)</button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </header>
      </React.Fragment>);
  
}