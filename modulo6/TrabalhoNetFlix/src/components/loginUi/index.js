import React, { useEffect, useState } from 'react';
import EpisodiosApi from '../../models/episodiosAPI';

import { Link, useHistory } from 'react-router-dom';

function LoginUi( ) {
  const history = useHistory();
  const [ login, setLogin ] = useState();
  const [ senha, setSenha ] = useState();
  const [ logar, setLogar ] = useState();

  useEffect( () => {
    if( login !== undefined && senha !== undefined ) {
      const episodiosApi = new EpisodiosApi();
      const requisicoes = [
        episodiosApi.buscarUsuario(login, senha)
      ];
  
      Promise.all( requisicoes )
        .then( respostas => {
          localStorage.setItem("user",respostas)
        })
      if(localStorage.getItem("user")){
        history.push('/')
      }

    }
  },[logar] );

  return (
    <>
      <h3>Entrar</h3>
      <input onBlur={ evt => setLogin( evt.target.value )} className="" type="text" name="login" placeholder="Login..."/>
      <input onBlur={ evt => setSenha( evt.target.value )} type="password" name="senha"/>
      <button onClick={ evt => setLogar( evt.target.value++ )} >Logar</button>
      <Link to='/formulario' ><button className="spaco-button">Cadastrar-se</button></Link>
    </>
  )
}

export default LoginUi;
