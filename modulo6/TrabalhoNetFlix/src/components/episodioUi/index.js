import React, { useState, useEffect } from "react";

import { Lista, BotaoUi } from '../../components';

import Estrelas from '../estrelasUi';

export default function EpisodioUi( props ) {

    const { episodio, qtdAssistido, objNota, registrarNota, marcarComoAssistido, detalhes } = props;
    const [ nome, setNome ] = useState( episodio.nome );
    const [ url, SetUrl ] = useState( episodio.url );
    const [ nota, setNota ] = useState( objNota / qtdAssistido );
    const [ imdb, setImdb ] = useState(detalhes.notaImdb);
    const [ sinopse, setSinopse ] = useState(detalhes.sinopse)
    const linha = ( item, i ) => {
      return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
    }

    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col col-sm-12">
              <div className="col col-sm-12">
                <h2>{ nome }</h2>
                <img src={ url } alt={ nome } />
              </div>
              <div className="col col-sm-6">
                <p>Nota imdb: { imdb }</p>
                <Estrelas
                  imdb={ nota }
                  permissao={ false }
                  registrarNota={ registrarNota }
                />
              </div>
              <p>{sinopse}</p>
              <div className="col col-sm-6">
                <ul>
                  <li><span>Vez assistido: { qtdAssistido } Vez(es)</span></li>
                  <li><span>Duração: { episodio.duracaoEmMin }</span></li>
                  <li><span>Temporada / Episódio: { episodio.temporadaEpisodio }</span></li>
                </ul>
                <Lista
                  classeName="botoes"
                  dados={ [
                    { cor: "azul", nome: "Já Assisti!", metodo: marcarComoAssistido }
                  ] }
                  funcao={ ( item, i ) => linha( item, i ) } />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
}