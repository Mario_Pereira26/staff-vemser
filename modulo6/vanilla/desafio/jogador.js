class Jogador {
    
    constructor(nome, numeroCamiseta){
        this._nome = nome;
        this._numero = numeroCamiseta;
        this._capitao = false;
    }

    get nome() {
        return this._nome;
    }

    get numero() {
        return this._numero;
    }

    virarCapitao = () => {
        this._capitao = true;
    }
}