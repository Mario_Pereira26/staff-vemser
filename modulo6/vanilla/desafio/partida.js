class Partida {
    
    constructor(mandante, visitante, placar){
        this._mandante = mandante;
        this._visitante = visitante;
        this._placar = placar;
    }

    get mandante() {
        return this._mandante;
    }

    get visitante() {
        return this._visitante;
    }

    get placar() {
        return this._placar;
    }

}