class Time {

    constructor(nome, tipoEsporte, ligaQueJoga){
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "ativo";
        this._ligaQueJoga = ligaQueJoga;
        this._jogadores = [];
        this._partidas = [];
    }

    adicionarJogadores = ( jogador ) => {
        this._jogadores.push(jogador);
    }

    buscarJogadoresPorNome = ( nomeJogador ) => {
        return this._jogadores.filter( jogador => jogador.nome == nomeJogador  );
    }

    buscarJogadoresPorNumero = ( numeroJogador ) => { 
        return this._jogadores.filter( jogador => jogador.numero == numeroJogador );
    }

    adicionarPartida = ( partida ) => {
        this._partidas.push(partida);
    }

    historicoPartidas = () => {
        return this._partidas;
    }

    mudarStatusTime = () => {
        this._status = !this._status;
    }
}