export default class Episodio {
  constructor( nome, duracao, temporada, ordemEpisodio, thumbUrl ) {
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.url = thumbUrl;
    this.qtdVezesAssistido = 0;
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`;
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2, 0 ) }/${ this.ordem.toString().padStart( 2, 0 ) }`;
  }

  avaliar( nota ) {
    this.nota = parseInt( nota );
  }

  marcarComoAssistido() {
    this.assistido = true;
    this.qtdVezesAssistido++;
  }
  
  validarNota( nota ) {
    return (nota >= 1 && nota <= 5);
  }
}