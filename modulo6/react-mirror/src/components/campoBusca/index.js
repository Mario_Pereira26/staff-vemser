import React from 'react';

const CampoBusca = ( { placeholder, atualizaValor } ) =>
  <React.Fragment>
    <input type="text" placeholder={ placeholder } onBlur={ atualizaValor } />
  </React.Fragment>

export default CampoBusca;
