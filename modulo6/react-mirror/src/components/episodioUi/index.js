import React, { Component } from "react";

export default class EpisodioUi extends Component {
  render(){
    const { episodio } = this.props;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col col-sm-12">
              <div className="col col-sm-12">
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome } />
              </div>
              <div className="col col-sm-12">
                <ul>
                  <li><span>Já Assisti: { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } Vez(es)</span></li>
                  <li><span>Duração: { episodio.duracaoEmMin }</span></li>
                  <li><span>Temporada / Episódio: { episodio.temporadaEpisodio }</span></li>
                  <li><span>Nota: { episodio.nota }</span></li>
                </ul> 
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}