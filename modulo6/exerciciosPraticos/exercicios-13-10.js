/*-------------Exercicio 01-------------*/
function calcularCirculo(raio,tipoCalculo){ 
    return tipoCalculo == "A" ? Math.trunc( (Math.PI*Math.pow(raio,2))) : Math.trunc(( 2*Math.PI*raio ) );
}

console.log(`Calculo de área de raio 2 resultado = ${calcularCirculo(5,"A")}`);
console.log(`Calculo de circunferência de raio 2 resultado = ${calcularCirculo(5,"C")}`);
/* Resolucao Marcos exercicio 01 */

let circulo = {
    raio: 3,
    tipoCalculo: "A"
}

function calcularCirculoMarcos( { raio, tipoCalculo:tipo }){
    return Math.ceil(tipo =="A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio);
}

console.log(`Marcos resultado = ${calcularCirculoMarcos(circulo)}`);

/*-------------Exercicio 02-------------*/
function naoBissexto(ano){
    return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0)) ? false : true;
}

console.log(`Resultado deve ser false = ${naoBissexto(2016)}`);
console.log(`Resultado deve ser true = ${naoBissexto(2017)}`);

/*-------------Exercicio 03-------------*/
function somarPares(arrayDeNumeros){
    let soma = 0;
    for (let i = 0; i < arrayDeNumeros.length; i+=2) {
        soma += arrayDeNumeros[i];      
    }
    return soma;
}

console.log(`Resultado de somarPares deve ser 3.34 = ${somarPares( [ 1, 56, 4.34, 6, -2 ] )}`);

/*-------------Exercicio 04-------------*/
function adicionar(numero){
    function add(numeroDentro){
        return numeroDentro+numero;
    }
    return add;
}

console.log(`Chamada da função adicionar(3)(4) deve ter resultado 7 = ${adicionar(3)(4)}`);
console.log(`Chamada da função adicionar(3)(4) deve ter resultado 14391 = ${adicionar(5642)(8749)}`);

/* Resolucao Marcos exercicio 04 */

let adicionarr = op1 => op2 => op1 + op2;

console.log(`Resposta Marcos = ${adicionarr(3)(4)}`);

/*-------------Exercicio 05-------------*/
/* Resolução esta no arquivo moedas.js */

/*---------------------Exemplos Dados Na Correção---------------------- */


const aula = {
    turma: 2020,
    qtdAulunos: 12,
    qtdAulasNoModulo(modulo) {
        switch (modulo) {
            case 1:
                console.log(10);
                break;
            case 2:
                console.log(5);
                break;
            default:
                break;
        }
    }
}

aula.qtdAulasNoModulo(1);

/*let is_divisivel = (divisor, numero) => !( numero % divisor );
const divisor = 2;
console.log(is_divisivel(divisor, 5));
console.log(is_divisivel(divisor, 8));
console.log(is_divisivel(divisor, 16));
console.log(is_divisivel(divisor, 9));
*/
const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
const divisor = 2;
console.log(is_divisivel(5));
console.log(is_divisivel(8));
console.log(is_divisivel(16));
console.log(is_divisivel(9));
