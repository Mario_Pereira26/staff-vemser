//import PropTypes from 'prop-types';

class ListaSeries {
	constructor(){
    
		this.series = JSON.parse(`[
      {
        "titulo": "Stranger Things",
        "anoEstreia": 2016,
        "diretor": [
          "Matt Duffer",
          "Ross Duffer"
        ],
        "genero": [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        "elenco": [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        "temporadas": 2,
        "numeroEpisodios": 17,
        "distribuidora": "Netflix"
      },
      {
        "titulo": "Game Of Thrones",
        "anoEstreia": 2011,
        "diretor": [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        "genero": [
          "Fantasia",
          "Drama"
        ],
        "elenco": [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        "temporadas": 7,
        "numeroEpisodios": 67,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The Walking Dead",
        "anoEstreia": 2010,
        "diretor": [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        "genero": [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        "elenco": [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        "temporadas": 9,
        "numeroEpisodios": 122,
        "distribuidora": "AMC"
      },
      {
        "titulo": "Band of Brothers",
        "anoEstreia": 20001,
        "diretor": [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        "genero": [
          "Guerra"
        ],
        "elenco": [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        "temporadas": 1,
        "numeroEpisodios": 10,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The JS Mirror",
        "anoEstreia": 2017,
        "diretor": [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        "genero": [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        "elenco": [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        "temporadas": 5,
        "numeroEpisodios": 40,
        "distribuidora": "DBC"
      },
      {
        "titulo": "Mr. Robot",
        "anoEstreia": 2018,
        "diretor": [
          "Sam Esmail"
        ],
        "genero": [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        "elenco": [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        "temporadas": 3,
        "numeroEpisodios": 32,
        "distribuidora": "USA Network"
      },
      {
        "titulo": "Narcos",
        "anoEstreia": 2015,
        "diretor": [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        "genero": [
          "Documentario",
          "Crime",
          "Drama"
        ],
        "elenco": [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        "temporadas": 3,
        "numeroEpisodios": 30,
        "distribuidora": null
      },
      {
        "titulo": "Westworld",
        "anoEstreia": 2016,
        "diretor": [
          "Athena Wickham"
        ],
        "genero": [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        "elenco": [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        "temporadas": 2,
        "numeroEpisodios": 20,
        "distribuidora": "HBO"
      },
      {
        "titulo": "Breaking Bad",
        "anoEstreia": 2008,
        "diretor": [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        "genero": [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        "elenco": [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        "temporadas": 5,
        "numeroEpisodios": 62,
        "distribuidora": "AMC"
      }
    ]`);

    console.log(this.series[5])
   
  }

  invalidas = () => {
    let ano = new Date();
        ano = ano.getFullYear();
        let titulosInvalidos = 'Series invalidas:'
        let titulos = this.series.map( serie => {
            let seForNull = false;
            Object.keys( serie ).forEach( function ( item ) {
                if ( serie[ item ] === null)
                seForNull = true
            } );
            if ( ( serie.anoEstreia > ano || seForNull ) ) {
                return ` ${serie.titulo} -`;
            }
        } );
        console.log( titulos.length)
        return titulosInvalidos + ( titulos.join( '' ).replace(/-*$/, "") );
        
  }

  filtrarPorAno = ano => {
    let seriesAnoEspecifico = this.series.filter( serie => serie.anoEstreia >= ano );
    return seriesAnoEspecifico;
  }

  procurarPorNome = nome => {
    let resultado = false;
    this.series.forEach( serie => {
      let validacao = serie.elenco.includes(nome);
      
      if(validacao){
        resultado = true;
      }
    })
    return resultado;
  }

  mediaDeEpisodios = () => {
    let totalDeEp = 0;
    const quantidade = this.series.length;
    this.series.map( serie => totalDeEp += serie.numeroEpisodios );
    return totalDeEp / quantidade;
  }

  totalSalarios = posicao => {
    const salarioDiretor = 100.000;
    const salarioOperarios = 40.000;
    const qtdDiretores = this.series[ posicao ].diretor.length;
    const qtdElenco = this.series[ posicao ].elenco.length;
    return ( salarioDiretor * qtdDiretores ) + ( salarioOperarios * qtdElenco );
  }

  queroGenero = ( genero ) => {
    let titulos = [];
    this.series.filter( serie => {
      let validacao = serie.genero.includes( genero );
      if( validacao ) {
        titulos.push(serie.titulo);
      }
    } )
    return titulos;
  }

  queroTitulo = ( titulo ) => {
    let titulos = [];
    this.series.filter( serie => {
      let validacao = serie.titulo.includes( titulo );
      if( validacao ) {
        titulos.push(serie.titulo);
      }
    } )
    return titulos;
  }

  creditos = ( posicao ) => {
    const titulo = this.series[posicao].titulo;
    
    let diretores = this.series[posicao].diretor.map( diretor => diretor.split( ' ' ).reverse().join( ' ' ) );
    diretores.sort();
    diretores = diretores.map(diretor => diretor.split(' ').reverse().join(' ') )
    
    let elenco = this.series[posicao].elenco.map( elenco => elenco.split( ' ' ).reverse().join( ' ' ) );
    elenco.sort();
    elenco = elenco.map(elenco => elenco.split(' ').reverse().join(' ') )
    return { titulo, diretores, elenco };
  }

  verificarAbreviacao = nome => nome.includes('. ');

  hashTag = () => {
    let hash = '#';
    this.series.forEach(serie => {
      let verificacao = serie.elenco.map( ator => this.verificarAbreviacao( ator ));

      if( !verificacao.includes(false) ){
        serie.elenco.map( ator => {
          let posicao = ator.indexOf('.') - 1;
          hash += ator.substr( posicao, 1 );
        } )
      }
    } );
    return hash;
  }

}
/*
ListaSeries.propTypes = {
  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  procurarPorNome: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  totalSalarios: PropTypes.array,
  queroGenero: PropTypes.array,
  queroTitulo: PropTypes.array,
  creditos: PropTypes.array,
  hashTag: PropTypes.array
}
*/
let series = new ListaSeries();
let exercicio1 = series.invalidas();
let exercicio2 = series.filtrarPorAno(2017);
let exercicio3False = series.procurarPorNome("Mario");
let exercicio3True = series.procurarPorNome("RJ Mitte");
let exercicio4 = series.mediaDeEpisodios();
let exercicio5 = series.totalSalarios(0);
let exercicioA6 = series.queroGenero("Caos");
let exercicioB6 = series.queroTitulo("The");
let exercicio7 = series.creditos(2);
let exercicio8 = series.hashTag();
console.log("//////////- 1 -////////////")
console.log(exercicio1)
console.log("//////////- 2 -////////////")
console.log(exercicio2)
console.log("//////////- 3 false -////////////")
console.log(exercicio3False)
console.log("//////////- 3 true -////////////")
console.log(exercicio3True)
console.log("//////////- 4 -////////////")
console.log(exercicio4)
console.log("//////////- 5 -////////////")
console.log(exercicio5)
console.log("//////////- 6A -///////////")
console.log(exercicioA6)
console.log("//////////- 6B -////////////")
console.log(exercicioB6)
console.log("//////////- 6 -////////////")
console.log(exercicio7)
console.log("//////////- 8 -////////////")
console.log(exercicio8)
console.log("//////////////////////")

