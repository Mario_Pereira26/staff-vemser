package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PagamentosControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clientesRepository;
    @Autowired
    private PacotesRepository pacotesRepository;
    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;
    @Autowired
    private EspacosRepository espacosRepository;
    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Test
    public void deveRetornar200QuandoConsultarPagamentos() throws Exception {
        URI uri = new URI("/api/pagamentos/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarPagamento() throws Exception {
        URI uri = new URI("/api/pagamentos/novo");
        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("eeail");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("tee@teft.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("teelefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("15548678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Jooquin");
        cliente.setCpf("17775699898");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);
        ClientesEntity newCliente = clientesRepository.save(cliente);

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(3);
        PacotesEntity newPacotes = pacotesRepository.save(pacote);

        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();

        clientePacote.setClientesPacotes(newCliente);
        clientePacote.setPacotesCli(newPacotes);
        clientePacote.setQuantidade(6);
        ClientesPacotesEntity newClientePacote = clientesPacotesRepository.save(clientePacote);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 18");
        espaco.setQtdPessoas(20);
        espaco.setValor(110.0);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspacosContrato(newEspaco);
        contratacao.setClientes(newCliente);
        contratacao.setTipoContratacao(TipoContratacaoEnum.TURNO);
        contratacao.setQuantidade(10);
        contratacao.setDesconto(4);
        contratacao.setPrazo(14);

        ContratacaoEntity newContratacao = contratacaoRepository.save(contratacao);

        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setClientesPacote(newClientePacote);
        pagamento.setContratacao(newContratacao);
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);

        String json = objectMapper.writeValueAsString(pagamento);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento").value("CREDITO")
        );
    }

    @Test
    public void deveRetornarUmPagamento() throws Exception{

        URI uri = new URI("/api/pagamentos/novo");
        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@teft.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("11148678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquin");
        cliente.setCpf("12345699898");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);
        ClientesEntity newCliente = clientesRepository.save(cliente);

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(3);
        PacotesEntity newPacotes = pacotesRepository.save(pacote);

        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();

        clientePacote.setClientesPacotes(newCliente);
        clientePacote.setPacotesCli(newPacotes);
        clientePacote.setQuantidade(6);
        ClientesPacotesEntity newClientePacote = clientesPacotesRepository.save(clientePacote);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 10");
        espaco.setQtdPessoas(20);
        espaco.setValor(110.0);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspacosContrato(newEspaco);
        contratacao.setClientes(newCliente);
        contratacao.setTipoContratacao(TipoContratacaoEnum.TURNO);
        contratacao.setQuantidade(10);
        contratacao.setDesconto(4);
        contratacao.setPrazo(14);

        ContratacaoEntity newContratacao = contratacaoRepository.save(contratacao);

        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setClientesPacote(newClientePacote);
        pagamento.setContratacao(newContratacao);
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);

        PagamentosEntity newPagamentos = pagamentosRepository.save(pagamento);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pagamentos/ver/{id}", newClientePacote.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento").value("CREDITO")
        );

    }
}
