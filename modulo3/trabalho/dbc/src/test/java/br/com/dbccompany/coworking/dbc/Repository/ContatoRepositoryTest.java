package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarContato(){

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tipoContato);
        contato.setValor("mario@gmail.com");

        repository.save(contato);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarPorValor(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tipoContato);
        contato.setValor("mario@gmail.com");

        repository.save(contato);

        assertEquals("mario@gmail.com", repository.findByValor("mario@gmail.com").getValor());
    }

    @Test
    public void buscarContatoNaoExistente(){

        String valor = "teste";
        assertNull(repository.findByValor(valor));
    }
}
