package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientesRepositoryTest {

    @Autowired ClientesRepository repository;

    @Test
    @Order(1)
    public void salvarClienteBuscarPorId(){

        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Mario");
        cliente.setCpf("12345678998");
        cliente.setContatos(contatos);
        cliente.setDataNascimento(new Date(1992,10,10));

        repository.save(cliente);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarClienteInexistente(){
        assertEquals(Optional.empty(),repository.findById(12));
        assertNull(repository.findByCpf("00000000000"));
        assertNull(repository.findByNomeAndCpf("Teste", "12345698710"));
        assertEquals(0, repository.findAllByDataNascimento(new Date(1992,10,10)).size());
    }

    @Test
    @Order(3)
    public void buscarClientePorCpf(){
        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Mario");
        cliente.setCpf("11111111111");
        cliente.setContatos(contatos);
        cliente.setDataNascimento(new Date(1992,10,10));

        repository.save(cliente);

        assertEquals("11111111111", repository.findByCpf("11111111111").getCpf());
    }

    @Test
    @Order(4)
    public void buscarClientePorNomeECpf(){
        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Henrique");
        cliente.setCpf("22111111111");
        cliente.setContatos(contatos);
        cliente.setDataNascimento(new Date(1992,10,10));

        repository.save(cliente);

        assertEquals("22111111111", repository.findByNomeAndCpf("Henrique", "22111111111").getCpf());
    }

    @Test
    @Order(5)
    public void buscarTodosClientesPorDataNascimento(){
        ClientesEntity cliente1 = new ClientesEntity();
        ClientesEntity cliente2 = new ClientesEntity();
        ClientesEntity cliente3 = new ClientesEntity();
        ClientesEntity cliente4 = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();
        Date data = new Date(1992,10,10);

        cliente1.setNome("Henrique");
        cliente1.setCpf("22112111111");
        cliente1.setContatos(contatos);
        cliente1.setDataNascimento(data);

        cliente2.setNome("Henrique 2");
        cliente2.setCpf("22221111111");
        cliente2.setContatos(contatos);
        cliente2.setDataNascimento(new Date(1994,10,10));

        cliente3.setNome("Henrique 3");
        cliente3.setCpf("22222211111");
        cliente3.setContatos(contatos);
        cliente3.setDataNascimento(new Date(1982,10,10));

        cliente4.setNome("Henrique 4");
        cliente4.setCpf("22111112222");
        cliente4.setContatos(contatos);
        cliente4.setDataNascimento(data);

        repository.save(cliente1);
        repository.save(cliente2);
        repository.save(cliente3);
        repository.save(cliente4);

        assertEquals(2,repository.findAllByDataNascimento(data).size());
    }

}
