package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SaldoClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clienteRepository;
    @Autowired
    private EspacosRepository espacosRepository;
    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Test
    public void deveRetornar200QuandoConsultarSaldoCliente() throws Exception {
        URI uri = new URI("/api/saldoCliente/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarRetornarUmSaldoCliente() throws Exception {
        URI uri = new URI("/api/saldoCliente/novo");

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("novo email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@test.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("novo telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("11145678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquina");
        cliente.setCpf("12345699998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("sala");
        espaco.setQtdPessoas(5);
        espaco.setValor(2);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        SaldoClientesEntityId id = new SaldoClientesEntityId();
        id.setIdClientes(newCliente.getId());
        id.setIdEspacos(newEspaco.getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(newCliente);
        saldoCliente.setEspaco(newEspaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento(new Date(System.currentTimeMillis()));

        String json = objectMapper.writeValueAsString(saldoCliente);
        json = json.replace("\"idClientes\":1","idCliente:{\"id\":1}");
        json = json.replace("\"idEspacos\":1","idEspacos:{\"id\":1}");
        json = json.replace("25/09/2020","2020-09-25");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(3)
        );
    }

    @Test
    public void deveRetornarUmSaldoCliente() throws Exception{

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("tel");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("32165478");
        ContatoEntity newContato = contatoRepository.save(contato);


        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Mark");
        cliente.setCpf("1999999998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("sala nova");
        espaco.setQtdPessoas(5);
        espaco.setValor(2);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        SaldoClientesEntityId id = new SaldoClientesEntityId();
        id.setIdClientes(newCliente.getId());
        id.setIdEspacos(newEspaco.getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(newCliente);
        saldoCliente.setEspaco(newEspaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldoCliente.setQuantidade(2);
        saldoCliente.setVencimento(new Date(System.currentTimeMillis()));

        SaldoClienteEntity newSaldoCliente = saldoClienteRepository.save(saldoCliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/saldoCliente/ver/{id}", newSaldoCliente.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(2)
        );
    }
}
