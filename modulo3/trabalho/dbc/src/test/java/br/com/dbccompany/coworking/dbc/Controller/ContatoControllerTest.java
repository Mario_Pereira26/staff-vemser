package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.ContatoRepository;
import br.com.dbccompany.coworking.dbc.Repository.TipoContatoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ContatoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;

    @Test
    public void deveRetornar200QuandoConsultarContato() throws Exception {
        URI uri = new URI("/api/contato/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void deveRetornar200QuandoAdicionarContato() throws Exception {
        URI uri = new URI("/api/contato/novo");

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");

        TipoContatoEntity newContato = tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newContato);
        contato.setValor("mario@gmail.com");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(contato))
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmContato() throws Exception {
        URI uri = new URI("/api/contato/novo");

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("telefone");

        TipoContatoEntity newContato = tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newContato);
        contato.setValor("98765432");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(objectMapper.writeValueAsString(contato))
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("98765432")
        );
    }

    @Test
    public void deveRetornarUmContato() throws Exception{

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("fixo");

        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();

        contato.setTipoContato(newTipoContato);
        contato.setValor("5136513298");

        ContatoEntity newContato = contatoRepository.save(contato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contato/ver/{id}", newContato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("5136513298")
        );

    }
}
