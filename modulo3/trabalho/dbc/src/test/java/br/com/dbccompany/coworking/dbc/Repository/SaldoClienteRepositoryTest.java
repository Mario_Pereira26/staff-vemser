package br.com.dbccompany.coworking.dbc.Repository;
import br.com.dbccompany.coworking.dbc.Entity.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SaldoClienteRepositoryTest {

    @Autowired
    SaldoClienteRepository repository;
    @Autowired
    EspacosRepository espacoRep;
    @Autowired
    ClientesRepository clienteRep;

    @Test
    @Order(1)
    public void salvarSaldoClienteBuscarPorId(){

        SaldoClientesEntityId id = new SaldoClientesEntityId();
        id.setIdClientes(1);
        id.setIdEspacos(1);

        ClientesEntity cliente = new ClientesEntity();
        EspacosEntity espaco = new EspacosEntity();
        espacoRep.save(espaco);
        clienteRep.save(cliente);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();

        saldoCliente.setId(id);
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento(new Date(1992,10,10));

        repository.save(saldoCliente);

        assertEquals(1, repository.findById(id).get().getId().getIdClientes());
    }

    @Test
    @Order(2)
    public void buscarSaldoClientesInexistentes(){
        SaldoClientesEntityId id = new SaldoClientesEntityId();
        id.setIdClientes(1);
        id.setIdEspacos(1);
        assertEquals(Optional.empty(),repository.findById(id));
    }

}
