package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.TipoContatoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TipoContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultarTipoContato() throws Exception {
        URI uri = new URI("/api/tipoContato/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmTipoContato() throws Exception {
        URI uri = new URI("/api/tipoContato/novo");
        String json = "{\"nome\":\"telefone\"}";
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.id").exists()
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome").value("telefone")
                );
    }

    @Test
    public void deveRetornarUmTipoContato() throws Exception{

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("novo telefone");

        TipoContatoEntity newUsuario = repository.save(tipoContato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/tipoContato/ver/{id}", newUsuario.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("novo telefone")
        );

    }
}
