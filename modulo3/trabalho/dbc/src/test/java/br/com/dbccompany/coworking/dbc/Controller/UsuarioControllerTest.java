package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.dbc.Repository.UsuarioRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UsuarioRepository repository;

    @Test
    public void deveRetornar200QuandoConsultarUsuario() throws Exception {
        URI uri = new URI("/api/usuario/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void deveRetornar200QuandoAdicionarUsuario() throws Exception {
        URI uri = new URI("/api/usuario/novo");

        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Teste");
        usuario.setEmail("teste@");
        usuario.setLogin("teste");
        usuario.setSenha("123456");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(usuario))
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuario/novo");
        String json = "{" +
                            "\"nome\" :\"Mario\"," +
                            "\"email\":\"mario@gmail\"," +
                            "\"login\":\"marioAdm\"," +
                            "\"senha\":\"123456\"" +
                      "}";
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.id").exists()
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome").value("Mario")
                );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception{

        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("NovoUsuario");
        usuario.setEmail("testedsa@");
        usuario.setLogin("testdsae");
        usuario.setSenha("123456");

        UsuarioEntity newUsuario = repository.save(usuario);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/usuario/ver/{id}", newUsuario.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("NovoUsuario")
        );

    }

}
