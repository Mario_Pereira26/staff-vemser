package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacosRepositoryTest {

    @Autowired
    EspacosRepository repository;

    @Test
    @Order(1)
    public void salvarEspacosBuscarPorId(){

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 1");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);

        repository.save(espaco);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarEspacosInexistente() {
        assertNull(repository.findByNome("Sala Londres"));
        assertNull(repository.findByQtdPessoasAndValor( 10,45.0));
        assertEquals(Optional.empty(), repository.findById(10));
    }

    @Test
    @Order(3)
    public void buscarEspacoPorQtdPessoasEValor(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 2");
        espaco.setQtdPessoas(110);
        espaco.setValor(100.0);

        repository.save(espaco);

        assertEquals("Sala 2",repository.findByQtdPessoasAndValor(110,100.0).getNome());
    }

    @Test
    @Order(4)
    public void buscarEspacoPorNome(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 3");
        espaco.setQtdPessoas(25);
        espaco.setValor(50.0);

        repository.save(espaco);

        assertEquals("Sala 3",repository.findByNome("Sala 3").getNome());
    }
}
