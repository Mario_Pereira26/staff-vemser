package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void salvarUsuario(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Mario");
        usuario.setEmail("mario@gmail.com");
        usuario.setLogin("mario");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals(usuario.getNome(),
                repository.findByNome("Mario").getNome()
        );
    }

    @Test
    public void buscarUsuarioPorUsernameEPassword(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Mario");
        usuario.setEmail("mario@gmail.com");
        usuario.setLogin("mario");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals(usuario.getNome(),
                repository.findByLoginAndSenha(
                        usuario.getUsername(),
                        usuario.getPassword()
                ).getNome()
        );
    }

    @Test
    public void buscarUsuarioNaoExistente(){
        String nome = "Henrique";

        assertNull(repository.findByNome(nome));
    }

    @Test
    public void buscarPorLogin(){
        UsuarioEntity usuario = new UsuarioEntity();

        usuario.setNome("Mario");
        usuario.setEmail("mario@gmail.com");
        usuario.setLogin("mario");
        usuario.setSenha("123456");

        repository.save(usuario);
        assertEquals("Mario",
                repository.findByLogin("mario").get().getNome()
        );
    }
}
