package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClientesPacotesControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clientesRepository;
    @Autowired
    private PacotesRepository pacotesRepository;
    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;

    @Test
    public void deveRetornar200QuandoConsultarClientesPacotes() throws Exception {
        URI uri = new URI("/api/clientesPacotes/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmClientesPacotes() throws Exception {
        URI uri = new URI("/api/clientesPacotes/novo");

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("novo email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@test.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("novo telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("11145678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquina");
        cliente.setCpf("12345699998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);
        ClientesEntity newCliente = clientesRepository.save(cliente);

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(2);
        PacotesEntity newPacotes = pacotesRepository.save(pacote);

        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();

        clientePacote.setClientesPacotes(newCliente);
        clientePacote.setPacotesCli(newPacotes);
        clientePacote.setQuantidade(5);

        String json = objectMapper.writeValueAsString(clientePacote);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value("5")
        );
    }

    @Test
    public void deveRetornarUmClientePacote() throws Exception{

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@teft.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("11148678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquin");
        cliente.setCpf("12345699898");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);
        ClientesEntity newCliente = clientesRepository.save(cliente);

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(3);
        PacotesEntity newPacotes = pacotesRepository.save(pacote);

        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();

        clientePacote.setClientesPacotes(newCliente);
        clientePacote.setPacotesCli(newPacotes);
        clientePacote.setQuantidade(6);

        ClientesPacotesEntity newClientePacote = clientesPacotesRepository.save(clientePacote);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientesPacotes/ver/{id}", newClientePacote.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(6)
        );

    }
}
