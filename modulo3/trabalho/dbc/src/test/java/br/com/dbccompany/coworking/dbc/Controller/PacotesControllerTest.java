package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Repository.PacotesRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PacotesControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PacotesRepository repository;

    @Test
    public void deveRetornar200QuandoConsultarPacotes() throws Exception {
        URI uri = new URI("/api/pacotes/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void deveRetornar200QuandoAdicionarPacote() throws Exception {
        URI uri = new URI("/api/pacotes/novo");

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(2);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(pacote))
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmPacote() throws Exception {
        URI uri = new URI("/api/pacotes/novo");

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(10);
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(objectMapper.writeValueAsString(pacote))
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("10")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception{

        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(22);

        PacotesEntity newPacotes = repository.save(pacote);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pacotes/ver/{id}", newPacotes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("22")
        );

    }
}
