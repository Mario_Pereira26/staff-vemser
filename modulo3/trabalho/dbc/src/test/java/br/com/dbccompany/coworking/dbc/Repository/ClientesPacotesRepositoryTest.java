package br.com.dbccompany.coworking.dbc.Repository;
import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientesPacotesRepositoryTest {
    @Autowired
    ClientesPacotesRepository repository;
    @Autowired
    ClientesRepository clientesRep;
    @Autowired
    PacotesRepository pacotesRep;

    @Test
    @Order(1)
    public void salvarClientesPacotesBuscarPorId(){
        ClientesEntity cliente = new ClientesEntity();
        PacotesEntity pacotes = new PacotesEntity();

        clientesRep.save(cliente);
        pacotesRep.save(pacotes);

        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();

        cliPacote.setClientesPacotes(cliente);
        cliPacote.setPacotesCli(pacotes);
        cliPacote.setQuantidade(5);

        repository.save(cliPacote);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarClientesPacotesInexistentes(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
