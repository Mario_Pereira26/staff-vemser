package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContratacaoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clienteRepository;
    @Autowired
    private EspacosRepository espacosRepository;
    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Test
    @Order(1)
    public void deveRetornar200QuandoConsultarContratacao() throws Exception {
        URI uri = new URI("/api/contratacao/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    @Order(2)
    public void salvarERetornarContratacao() throws Exception {
        URI uri = new URI("/api/contratacao/novo");
        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("no email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@tt.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("notelefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("15445678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquinaa");
        cliente.setCpf("13345699998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 5");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspacosContrato(newEspaco);
        contratacao.setClientes(newCliente);
        contratacao.setTipoContratacao(TipoContratacaoEnum.TURNO);
        contratacao.setQuantidade(3);
        contratacao.setDesconto(2);
        contratacao.setPrazo(10);

        String json = objectMapper.writeValueAsString(contratacao);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(3)
        );

    }

    @Test
    @Order(3)
    public void deveRetornarUmaContratacao() throws Exception{

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("teee@tt.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquin");
        cliente.setCpf("44345699998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 10");
        espaco.setQtdPessoas(20);
        espaco.setValor(110.0);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setEspacosContrato(newEspaco);
        contratacao.setClientes(newCliente);
        contratacao.setTipoContratacao(TipoContratacaoEnum.TURNO);
        contratacao.setQuantidade(10);
        contratacao.setDesconto(4);
        contratacao.setPrazo(14);

        ContratacaoEntity newContratacao = contratacaoRepository.save(contratacao);

        String json = objectMapper.writeValueAsString(newContratacao);
        json = json.replace("\"contratos\":null","\"contratos\":1");
        System.out.println("////////////aquuuuuiiii//////////////");
        System.out.println(json);
        System.out.println("//////////////////////////");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contratacao/ver/{id}", newContratacao.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(10)
        );

    }
}
