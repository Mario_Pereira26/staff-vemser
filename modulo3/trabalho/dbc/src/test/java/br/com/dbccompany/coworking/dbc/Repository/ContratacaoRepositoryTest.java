package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContratacaoRepositoryTest {
    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacosRepository espacoRepository;

    @Autowired
    private ClientesRepository clienteRepository;

    @Test
    @Order(1)
    public void salvarContratacao() {

        ClientesEntity cliente = new ClientesEntity();
        EspacosEntity espaco = new EspacosEntity();

        ContratacaoEntity contratacao = new ContratacaoEntity();

        clienteRepository.save(cliente);
        espacoRepository.save(espaco);

        contratacao.setEspacosContrato(espaco);
        contratacao.setClientes(cliente);
        contratacao.setTipoContratacao(TipoContratacaoEnum.HORA);
        contratacao.setQuantidade(5);
        contratacao.setDesconto(1);
        contratacao.setPrazo(2);

        repository.save(contratacao);

        assertEquals(1, repository.findById(1).get().getId());

    }

    @Test
    @Order(2)
    public void buscarContratacaoInexistente(){
        assertEquals(Optional.empty(),repository.findById(11));
        assertTrue(repository.findAllByTipoContratacao(TipoContratacaoEnum.SEMANA).isEmpty());
        assertNull(repository.findByPrazo(1000));
    }

    @Test
    @Order(3)
    public  void buscarContratacaoPorTipoContratacao(){

        // clientes
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("blabla");
        cliente.setDataNascimento(new Date(1992,10,10));
        cliente.setCpf("12365478999");

        ClientesEntity newCliente = clienteRepository.save(cliente);

        // espaco
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("sala");
        espaco.setQtdPessoas(5);
        espaco.setValor(2);

        //contratacao
        ContratacaoEntity contratacao1 = new ContratacaoEntity();
        contratacao1.setClientes(newCliente);
        contratacao1.setEspacosContrato(espacoRepository.save(espaco));
        contratacao1.setTipoContratacao(TipoContratacaoEnum.HORA);
        contratacao1.setQuantidade(5);
        contratacao1.setDesconto(1);
        contratacao1.setPrazo(2);

        ContratacaoEntity contratacao2 = new ContratacaoEntity();
        contratacao2.setClientes(newCliente);
        contratacao2.setEspacosContrato(espacoRepository.save(espaco));
        contratacao2.setTipoContratacao(TipoContratacaoEnum.SEMANA);
        contratacao2.setQuantidade(5);
        contratacao2.setDesconto(1);
        contratacao2.setPrazo(2);

        ContratacaoEntity contratacao3 = new ContratacaoEntity();
        contratacao3.setClientes(newCliente);
        contratacao3.setEspacosContrato(espacoRepository.save(espaco));
        contratacao3.setTipoContratacao(TipoContratacaoEnum.HORA);
        contratacao3.setQuantidade(5);
        contratacao3.setDesconto(1);
        contratacao3.setPrazo(2);

        repository.save(contratacao1);
        repository.save(contratacao2);
        repository.save(contratacao3);

        assertEquals(2, repository.findAllByTipoContratacao(TipoContratacaoEnum.HORA).size());
    }


}
