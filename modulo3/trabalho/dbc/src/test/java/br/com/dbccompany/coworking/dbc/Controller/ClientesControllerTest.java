package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.ClientesRepository;
import br.com.dbccompany.coworking.dbc.Repository.ContatoRepository;
import br.com.dbccompany.coworking.dbc.Repository.TipoContatoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClientesControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clienteRepository;

    @Test
    public void deveRetornar200QuandoConsultarClientes() throws Exception {
        URI uri = new URI("/api/clientes/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarRetornarUmClienteEntity() throws Exception {

        URI uri = new URI("/api/clientes/novo");
        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("teste@test.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("12345678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Teste");
        cliente.setCpf("12345678998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        String jsonContatos = objectMapper.writeValueAsString(cliente);
        jsonContatos = jsonContatos.replace("\"id\":null,","");
        jsonContatos = jsonContatos.replace("25/09/2020","2020-09-25");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(jsonContatos)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.id").exists()
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome").value("Teste")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception{

        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("novo email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("te@test.com");
        ContatoEntity newContato = contatoRepository.save(contato);

        // segundo tipo contato
        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("novo telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();

        // segundo contato
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("11145678");
        ContatoEntity newContato2 = contatoRepository.save(contato2);

        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);
        lstContatos.add(newContato2);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Joaquina");
        cliente.setCpf("12345699998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientes/ver/{id}", newCliente.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Joaquina")
        );

    }
}
