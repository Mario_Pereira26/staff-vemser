package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.dbc.Repository.EspacosPacotesRepository;
import br.com.dbccompany.coworking.dbc.Repository.EspacosRepository;
import br.com.dbccompany.coworking.dbc.Repository.PacotesRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacosPacotesControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PacotesRepository pacotesRepository;
    @Autowired
    private EspacosRepository espacosRepository;
    @Autowired
    private EspacosPacotesRepository espacosPacoteRepository;

    @Test
    @Order(1)
    public void deveRetornar200QuandoConsultarEspacosPacotes() throws Exception {
        URI uri = new URI("/api/espacosPacotes/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    @Order(2)
    public void salvarERetornarUmEspacoPacote() throws Exception {
        URI uri = new URI("/api/espacosPacotes/novo");

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 1");
        espaco.setQtdPessoas(10);
        espaco.setValor(10.0);
        EspacosEntity newEspaco  = espacosRepository.save(espaco);

        PacotesEntity pacotes = new PacotesEntity();
        pacotes.setValor(10);
        PacotesEntity newPacotes = pacotesRepository.save(pacotes);

        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        espacosPacotes.setEspacos(newEspaco);
        espacosPacotes.setPacotes(newPacotes);
        espacosPacotes.setTipoContratacao(TipoContratacaoEnum.SEMANA);
        espacosPacotes.setQuantidade(5);
        espacosPacotes.setPrazo(3);
        String json = objectMapper.writeValueAsString(espacosPacotes);
        json = json.replace("\"id\":null,","");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao").value("SEMANA")
        );
    }

    @Test
    @Order(3)
    public void deveRetornarUmEspacoPacote() throws Exception{

        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 2");
        espaco.setQtdPessoas(120);
        espaco.setValor(120.0);
        EspacosEntity newEspaco  = espacosRepository.save(espaco);

        PacotesEntity pacotes = new PacotesEntity();
        pacotes.setValor(120);
        PacotesEntity newPacotes = pacotesRepository.save(pacotes);

        espacosPacotes.setEspacos(newEspaco);
        espacosPacotes.setPacotes(newPacotes);
        espacosPacotes.setTipoContratacao(TipoContratacaoEnum.SEMANA);
        espacosPacotes.setQuantidade(25);
        espacosPacotes.setPrazo(30);

        EspacosPacotesEntity newEspacoPacotes = espacosPacoteRepository.save(espacosPacotes);
        System.out.println("//////////////////////////");
        System.out.println(objectMapper.writeValueAsString(newEspacoPacotes));
        System.out.println("//////////////////////////");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacosPacotes/ver/{id}", newEspacoPacotes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao").value("SEMANA")
        );

    }
}
