package br.com.dbccompany.coworking.dbc.Repository;
import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacosPacotesRepositoryTest {

    @Autowired
    EspacosRepository espacoRep;
    @Autowired
    PacotesRepository pacoteRep;
    @Autowired
    EspacosPacotesRepository repository;

    @Test
    @Order(1)
    public void salvarEspacosPacotesBuscarPorId(){
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        EspacosEntity espaco = new EspacosEntity();
        PacotesEntity pacotes = new PacotesEntity();

        espacoRep.save(espaco);
        pacoteRep.save(pacotes);

        espacosPacotes.setEspacos(espaco);
        espacosPacotes.setPacotes(pacotes);
        espacosPacotes.setTipoContratacao(TipoContratacaoEnum.HORA);
        espacosPacotes.setQuantidade(5);
        espacosPacotes.setPrazo(3);

        repository.save(espacosPacotes);

        assertEquals(1, repository.findById(1).get().getId());

    }

    @Test
    @Order(2)
    public void buscarEspacosPacotesInexistentes(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
