package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PagamentosRepositoryTest {

    @Autowired
    private PagamentosRepository repository;

    @Autowired
    private ContratacaoRepository contratacaoRep;

    @Autowired
    private ClientesPacotesRepository cliPacotesRep;

    @Autowired
    private EspacosRepository espacpRep;

    @Autowired
    private ClientesRepository cliRepository;

    @Autowired
    private PacotesRepository pacoteRep;

    @Test
    @Order(1)
    public void salvarPagamentoBuscarPorId(){
        PagamentosEntity pagamento = new PagamentosEntity();

        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();
        ContratacaoEntity contratacao = new ContratacaoEntity();

        cliPacotesRep.save(cliPacote);
        contratacaoRep.save(contratacao);

        pagamento.setClientesPacote(cliPacote);
        pagamento.setContratacao(contratacao);
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);

        repository.save(pagamento);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    @Order(2)
    public void buscarPagamentoInexistente(){
        assertFalse(repository.findById(2).isPresent());
    }

    @Test
    @Order(3)
    public void buscarTodosPagamentosPorTipo(){
        PagamentosEntity pagamento = new PagamentosEntity();

        //pacote
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(12);

        // cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("blabla");
        cliente.setDataNascimento(new Date(1992,10,10));
        cliente.setCpf("12365478999");

        ClientesEntity newCliente = cliRepository.save(cliente);

        // espaco
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("sala");
        espaco.setQtdPessoas(5);
        espaco.setValor(2);

        // clientePacote
        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();
        cliPacote.setClientesPacotes(newCliente);
        cliPacote.setPacotesCli(pacoteRep.save(pacote));
        cliPacote.setQuantidade(2);

        // contratacao
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setClientes(newCliente);
        contratacao.setEspacosContrato(espacpRep.save(espaco));
        contratacao.setTipoContratacao(TipoContratacaoEnum.HORA);
        contratacao.setQuantidade(5);
        contratacao.setDesconto(1);
        contratacao.setPrazo(2);

        // pagamento
        pagamento.setClientesPacote(cliPacotesRep.save(cliPacote));
        pagamento.setContratacao(contratacaoRep.save(contratacao));
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);

        repository.save(pagamento);

        assertEquals(1, repository.findAllByTipoPagamento(TipoPagamentoEnum.CREDITO).size());
    }

}
