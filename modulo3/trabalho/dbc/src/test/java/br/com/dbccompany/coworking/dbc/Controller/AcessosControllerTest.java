package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AcessosControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ClientesRepository clienteRepository;
    @Autowired
    private EspacosRepository espacosRepository;
    @Autowired
    private SaldoClienteRepository saldoClienteRepository;
    @Autowired
    private AcessosEntity acessosEntity;

    @Test
    @Order(1)
    public void deveRetornar200QuandoConsultarAcessos() throws Exception {
        URI uri = new URI("/api/acessos/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    @Order(2)
    public void salvarRetornarUmAcesso() throws Exception {
        URI uri = new URI("/api/acessos/novo");
        //primeiro tipo contato
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("tel");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();

        // primeiro contato
        contato.setTipoContato(newTipoContato);
        contato.setValor("32165478");
        ContatoEntity newContato = contatoRepository.save(contato);


        // adicionar na lista
        List<ContatoEntity> lstContatos = new ArrayList<>();
        lstContatos.add(newContato);

        // cria cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Mark");
        cliente.setCpf("1999999998");
        cliente.setDataNascimento(new Date(System.currentTimeMillis()));
        cliente.setContatos(lstContatos);

        ClientesEntity newCliente = clienteRepository.save(cliente);

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("sala nova");
        espaco.setQtdPessoas(5);
        espaco.setValor(2);
        EspacosEntity newEspaco = espacosRepository.save(espaco);

        SaldoClientesEntityId id = new SaldoClientesEntityId();
        id.setIdClientes(newCliente.getId());
        id.setIdEspacos(newEspaco.getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(newCliente);
        saldoCliente.setEspaco(newEspaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldoCliente.setQuantidade(2);
        saldoCliente.setVencimento(new Date(System.currentTimeMillis()));

        SaldoClienteEntity newSaldoCliente = saldoClienteRepository.save(saldoCliente);

        AcessosEntity acesso = new AcessosEntity();
        acesso.setSaldoCliente(newSaldoCliente);
        acesso.setEntrada(true);
        acesso.setExcecao(false);
        acesso.setData(new Date(System.currentTimeMillis()));

        String json = objectMapper.writeValueAsString(acesso);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.isExecao").value(false)
        );
    }
}
