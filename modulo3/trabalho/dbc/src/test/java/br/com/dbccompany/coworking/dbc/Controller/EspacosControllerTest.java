package br.com.dbccompany.coworking.dbc.Controller;
import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Repository.EspacosRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacosControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EspacosRepository repository;

    @Test
    @Order(1)
    public void deveRetornar200QuandoConsultarEspacos() throws Exception {
        URI uri = new URI("/api/espacos/todos");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }


    @Test
    @Order(2)
    public void salvarERetornarUmEspaco() throws Exception {
        URI uri = new URI("/api/espacos/novo");
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 2");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);
        String json = objectMapper.writeValueAsString(espaco);
        json = json.replace("\"id\":null,","");
        System.out.println("////////////////");
        System.out.println(json);
        System.out.println("////////////////");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Sala 2")
        );
    }

    @Test
    @Order(3)
    public void deveRetornarUmEspaco() throws Exception{

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 3");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);

        EspacosEntity newEspaco = repository.save(espaco);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacos/ver/{id}", newEspaco.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Sala 3")
        );

    }
}
