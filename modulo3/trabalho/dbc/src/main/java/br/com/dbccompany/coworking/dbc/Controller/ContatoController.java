package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Service.ContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/api/contato") // rota
public class ContatoController extends AbstractController<ContatoService, ContatoEntity, Integer> {
}
