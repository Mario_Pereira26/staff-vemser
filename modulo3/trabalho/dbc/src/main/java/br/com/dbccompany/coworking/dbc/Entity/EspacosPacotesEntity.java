package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
public class EspacosPacotesEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_ESPACOS")
    private EspacosEntity espacos;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_PACOTES")
    private PacotesEntity pacotes;

    @Enumerated( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    private int quantidade;

    private int prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
