package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Repository.EspacosRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosService extends ServiceAbstract<EspacosRepository, EspacosEntity, Integer> {
}
