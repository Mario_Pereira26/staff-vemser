package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.dbc.DbcApplication;
import br.com.dbccompany.coworking.dbc.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.dbc.Service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
@Controller
@RequestMapping("/api/usuario") // rota
public class UsuarioController {

    private Logger logger = LoggerFactory.getLogger(DbcApplication.class);

    @Autowired
    UsuarioService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<UsuarioDTO> todosUsuarios() {
        logger.info("Iniciando buscar todos usuarios!");
        return service.retornarListaUsuarios();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO user) throws NoSuchAlgorithmException {
        logger.info("Todos os campos devem ser informados!");
        UsuarioEntity usuarioEntity = user.convert();
        UsuarioDTO newDto = new UsuarioDTO(service.salvar(usuarioEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody

    public UsuarioEntity usuariosEspecifico(@PathVariable Integer id){
        logger.info("Deve ser informado o id para buscar usuario!");
        return service.porId(id);
    }

    // usando DTO
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public UsuarioDTO editarUsuarios(@PathVariable Integer id, @RequestBody UsuarioDTO usuario){
        logger.info("Deve ser informado o id para editar usuario!");
        UsuarioEntity usuarioEntity = usuario.convert();
        UsuarioDTO usuarioDTO = new UsuarioDTO(service.editar(usuarioEntity, id));
        return usuarioDTO;
    }

    @PostMapping(value = "/login" )
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuarioDTO login) throws NoSuchAlgorithmException {
        logger.info("Deve ser informado login e senha para logar usuario!");
        return service.login(login.getLogin(), login.getSenha());
    }
}
