package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.EspacosPacotesDTO;
import br.com.dbccompany.coworking.dbc.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.dbc.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
@Controller
@RequestMapping("/api/espacosPacotes") // rota
public class EspacosPacotesController {
    @Autowired
    EspacosPacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosPacotesEntity> todosPaisEspacosPacote() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosPacotesDTO salvar(@RequestBody EspacosPacotesDTO espacoPacote) throws NoSuchAlgorithmException {
        EspacosPacotesEntity espacoPacoteEntity = espacoPacote.convert();
        EspacosPacotesDTO newDto = new EspacosPacotesDTO(service.salvar(espacoPacoteEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EspacosPacotesEntity EspacosPacotesEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotesEntity editarEspacoPacotes(@PathVariable Integer id, @RequestBody EspacosPacotesEntity espacoPacotes){
        return service.editar(espacoPacotes, id);
    }
}
