package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;

public class PacotesDTO {
    private Integer id;
    private int valor;

    public PacotesDTO() {}

    public PacotesDTO(PacotesEntity pacote) {
        this.id = pacote.getId();
        this.valor = pacote.getValor();
    }

    public PacotesEntity convert(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setId(this.id);
        pacote.setValor(this.valor);

        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
