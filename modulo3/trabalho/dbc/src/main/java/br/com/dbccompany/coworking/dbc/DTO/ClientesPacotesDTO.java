package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;

public class ClientesPacotesDTO {

    private int id;
    private ClientesEntity clientes;
    private PacotesEntity pacotes;
    private int quantidade;

    public ClientesPacotesDTO() {}

    public ClientesPacotesDTO(ClientesPacotesEntity clientePacote) {
        this.id = clientePacote.getId();
        this.clientes = clientePacote.getClientesPacotes();
        this.pacotes = clientePacote.getPacotesCli();
        this.quantidade = clientePacote.getQuantidade();
    }

    public ClientesPacotesEntity convert(){
        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();
        clientePacote.setId(this.id);
        clientePacote.setClientesPacotes(this.clientes);
        clientePacote.setPacotesCli(this.pacotes);
        clientePacote.setQuantidade(this.quantidade);
        return clientePacote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
