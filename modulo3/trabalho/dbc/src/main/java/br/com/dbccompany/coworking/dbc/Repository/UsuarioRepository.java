package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    UsuarioEntity findByLoginAndSenha( String login, String senha);
    UsuarioEntity findByNome(String nome);
    Optional<UsuarioEntity> findByLogin(String login);
}
