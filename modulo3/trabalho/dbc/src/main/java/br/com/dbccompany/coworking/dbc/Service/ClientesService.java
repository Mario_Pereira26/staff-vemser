package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DTO.ClientesDTO;
import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.ClientesRepository;
import br.com.dbccompany.coworking.dbc.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientesService extends ServiceAbstract<ClientesRepository, ClientesEntity, Integer> {

    @Autowired
    ContatoRepository contatoRepository;

    public ClientesDTO salvarComContatos(ClientesDTO cliente){

        List<ContatoEntity> contato = new ArrayList<>();

        try {
            contato.add(contatoRepository.findByValor("email"));
        }catch (Exception e){
            logger.error("Erro em encontrar contato com valor email: " + e.getMessage());
            throw new RuntimeException();
        }

        try {
            contato.add(contatoRepository.findByValor("telefone"));
        }catch (Exception e){
            logger.error("Erro em encontrar contato com valor telefone: " + e.getMessage());
            throw new RuntimeException();
        }

        cliente.setContatos(contato);
        ClientesEntity clienteFinal = cliente.convert();

        try {
            logger.warn("Para salvar cliente deve ter todos os parametros prenechidos!");
            ClientesDTO newDTO = new ClientesDTO(repository.save(clienteFinal));
            return newDTO;
        }catch (Exception e){
            logger.error("Erro em salvar clienteFinal com contatos: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    private ContatoEntity validacaoContato(String nome){
        try{
            logger.warn("Validando contato, se contato não existir ele cria um novo");
            ContatoEntity contato = contatoRepository.findByValor(nome);
            if(contato == null){
                contato = contatoRepository.save((new ContatoEntity()));
            }
            return contato;
        }catch (Exception e){
            logger.error("Erro em validar contato: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity salvar(ClientesEntity cliente){
        boolean peloMenosUmEmail = false;
        boolean peloMenosUmTelefone = false;
        List<ContatoEntity> contatos = cliente.getContatos();
        for (ContatoEntity contato : contatos){
            String tipoContato = contato.getTipoContato().getNome();
            if(tipoContato.equalsIgnoreCase("Email") && peloMenosUmEmail == false){
                peloMenosUmEmail = true;
            }
            if(tipoContato.equalsIgnoreCase("Telefone") && peloMenosUmTelefone == false){
                peloMenosUmTelefone = true;
            }
        }

        if(peloMenosUmEmail == true && peloMenosUmTelefone == true){
            try {
                return repository.save(cliente);
            }catch (Exception e){
                logger.error("Erro em salvar cliente que contem dois contatos: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        List<ContatoEntity> contatosNovos = new ArrayList<>();
        TipoContatoEntity tipo = new TipoContatoEntity();
        ContatoEntity contatoTelefone = new ContatoEntity();
        ContatoEntity contatoEmail = new ContatoEntity();

        tipo.setNome("Telefone");
        contatoTelefone.setTipoContato(tipo);
        contatosNovos.add(contatoTelefone);

        tipo.setNome("Email");
        contatoTelefone.setTipoContato(tipo);
        contatosNovos.add(contatoTelefone);

        cliente.setContatos(contatosNovos);

        try {
            return repository.save(cliente);
        }catch (Exception e){
            logger.error("Erro em salvar cliente que com no minimo dois contatos: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
