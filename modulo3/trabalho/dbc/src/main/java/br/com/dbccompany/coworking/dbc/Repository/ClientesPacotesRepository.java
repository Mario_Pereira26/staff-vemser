package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotesEntity, Integer> {

}
