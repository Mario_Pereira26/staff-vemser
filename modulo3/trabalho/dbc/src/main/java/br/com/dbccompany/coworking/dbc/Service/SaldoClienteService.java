package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DbcApplication;
import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClientesEntityId;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.dbc.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Date;
import java.util.Calendar;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository,
                                         SaldoClienteEntity, SaldoClientesEntityId> {

    private Logger logger = LoggerFactory.getLogger(DbcApplication.class);

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity salvar(ContratacaoEntity contratacao){

        int idCliente = contratacao.getClientes().getId();
        int idEspaco = contratacao.getEspacosContrato().getId();

        SaldoClientesEntityId id = new SaldoClientesEntityId(idCliente,idEspaco);
        SaldoClienteEntity saldoCliente = this.buscar(id);

        if(saldoCliente == null){
            saldoCliente.setId(id);
        }

        Calendar calendario = Calendar.getInstance();
        if(saldoCliente.getVencimento() == null){
            saldoCliente.setVencimento((Date) calendario.getTime());
        }

        calendario.setTime(saldoCliente.getVencimento());

        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.MINUTO)){
            calendario.add(Calendar.MINUTE,contratacao.getQuantidade()+ contratacao.getPrazo());
        }
        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.HORA)){
            calendario.add(Calendar.HOUR,contratacao.getQuantidade()+ contratacao.getPrazo());
        }
        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.DIARIA)){
            calendario.add(Calendar.DAY_OF_YEAR,contratacao.getQuantidade()+ contratacao.getPrazo());
        }
        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.TURNO)){
            calendario.add(Calendar.HOUR,(contratacao.getQuantidade()*5)+ contratacao.getPrazo());
        }
        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.SEMANA)){
            calendario.add(Calendar.WEEK_OF_YEAR,contratacao.getQuantidade() + contratacao.getPrazo());
        }
        if(contratacao.getTipoContratacao().equals(TipoContratacaoEnum.MES)){
            calendario.add(Calendar.MONTH,contratacao.getQuantidade() + contratacao.getPrazo());
        }

        saldoCliente.setVencimento((Date) calendario.getTime());
        saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());

        logger.info("Se estiver faltando parametros não ira salvar SaldoClienteEntity!");
        try {
            logger.warn("Salvando SaldoClienteEntity!");
            return repository.save(saldoCliente);
        }catch (Exception e){
            logger.error("Ocorreu um erro na inserção de SaldoClienteEntity: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
