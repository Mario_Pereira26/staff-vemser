package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
public class ClientesEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(unique = true,nullable = false,length = 12)
    private String cpf;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataNascimento;
    @JsonManagedReference
    @OneToMany(mappedBy = "cliente")
    private List<ContatoEntity> contatos;
    @JsonManagedReference
    @OneToMany(mappedBy = "clientesPacotes")
    private List<ClientesPacotesEntity> clientesPacotes;
    @JsonManagedReference
    @OneToMany(mappedBy = "clientes")
    private List<ContratacaoEntity> contratos;
    @JsonManagedReference
    @OneToMany(mappedBy = "cliente")
    private List<SaldoClienteEntity> clientes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getContratos() {
        return contratos;
    }

    public void setContratos(List<ContratacaoEntity> contratos) {
        this.contratos = contratos;
    }

    public List<SaldoClienteEntity> getClientes() {
        return clientes;
    }

    public void setClientes(List<SaldoClienteEntity> clientes) {
        this.clientes = clientes;
    }

}
