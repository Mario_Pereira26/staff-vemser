package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;

public class EspacosDTO {
    private Integer id;
    private String nome;
    private Integer qtdPessoas;
    private double valor;

    public EspacosDTO() {}

    public EspacosDTO(EspacosEntity espaco) {
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor();
    }

    public EspacosEntity convert(){

        EspacosEntity espaco = new EspacosEntity();

        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.valor);

        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
