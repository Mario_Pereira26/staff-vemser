package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.ClientesDTO;
import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import br.com.dbccompany.coworking.dbc.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes") // rota
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping (value = "/todos" )
    @ResponseBody
    public List<ClientesEntity> todosClientes() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesDTO salvar(@RequestBody ClientesDTO cliente){
        ClientesEntity clienteEntity = cliente.convert();
        ClientesDTO newDto = new ClientesDTO(service.salvar(clienteEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesEntity clienteEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesEntity editarClientes(@PathVariable Integer id, @RequestBody ClientesEntity cliente){
        return service.editar(cliente, id);
    }

}
