package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Service.PagamentosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/pagamentos") // rota
public class PagamentosController extends AbstractController<PagamentosService, PacotesEntity,Integer> {
}
