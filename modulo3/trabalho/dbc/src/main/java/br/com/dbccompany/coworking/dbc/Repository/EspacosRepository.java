package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {

    EspacosEntity findByQtdPessoasAndValor(int qtdPessoas, double valor);
    EspacosEntity findByNome(String nome);
    Optional<EspacosEntity> findById(Integer id);

}
