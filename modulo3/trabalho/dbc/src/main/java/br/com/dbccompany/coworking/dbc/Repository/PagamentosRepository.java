package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {
    List<PagamentosEntity> findAllByTipoPagamento(TipoPagamentoEnum tipoPagamento);
}
