package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.dbc.Repository.EspacosPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosPacotesService extends ServiceAbstract<EspacosPacotesRepository, EspacosPacotesEntity, Integer> {
}
