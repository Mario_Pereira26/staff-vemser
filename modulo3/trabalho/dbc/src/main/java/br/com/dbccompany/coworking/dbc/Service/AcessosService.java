package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DTO.AcessosDTO;
import br.com.dbccompany.coworking.dbc.Entity.AcessosEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClientesEntityId;
import br.com.dbccompany.coworking.dbc.Repository.AcessosRepository;
import br.com.dbccompany.coworking.dbc.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;


@Service
public class AcessosService extends ServiceAbstract<AcessosRepository,AcessosEntity, Integer> {

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessosDTO salvarEntrada(AcessosDTO acesso) throws NoSuchAlgorithmException {
        if(acesso.isEntrada()){
            SaldoClientesEntityId id = acesso.getClienteSaldoCliente().getId();
            try {
                logger.warn("Iniciando busca do saldo por id!");
                SaldoClienteEntity saldo = saldoRepository.findById(id).get();
                if (saldo.getQuantidade() <= 0) {
                    return null;
                }
            }catch (Exception e){
                logger.error("Erro ao salvar acesso, não encontrou saldo por id: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        AcessosEntity acessosEntity = acesso.convert();
        AcessosDTO newDTO = new AcessosDTO(super.salvar(acessosEntity));
        return newDTO;
    }
}
