package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Repository.ContratacaoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity,Integer> {

}
