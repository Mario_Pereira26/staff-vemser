package br.com.dbccompany.coworking.dbc.Repository;
;
import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    Optional<ContratacaoEntity> findById(Integer id);
    List<ContratacaoEntity> findAllByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    ContratacaoEntity findByPrazo(Integer prazo);
}
