package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository, TipoContatoEntity, Integer> {
}
