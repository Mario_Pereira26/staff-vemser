package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTES")
    @JsonBackReference
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO")
    @JsonBackReference
    private EspacosEntity espacosContrato;

    @Column(name = "TIPO_CONTRATACAO")
    @Enumerated( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;
    @Column(nullable = false)
    private int desconto;
    @Column(nullable = false)
    private int prazo;

    @OneToMany(mappedBy = "contratacao")
    @JsonManagedReference
    private List<PagamentosEntity> pagamentos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspacosContrato() {
        return espacosContrato;
    }

    public void setEspacosContrato(EspacosEntity espacosContrato) {
        this.espacosContrato = espacosContrato;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
