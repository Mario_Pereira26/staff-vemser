package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Service.EspacosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/espacos") // rota
public class EspacosController extends AbstractController<EspacosService, EspacosEntity,Integer> {
}
