package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;

public class ContatoDTO {
    private Integer id;
    private TipoContatoEntity tipoContato;
    private String valor;

    public ContatoDTO() {}

    public ContatoDTO(ContatoEntity contato) {
        this.id = contato.getId();
        this.tipoContato = contato.getTipoContato();
        this.valor = contato.getValor();
    }

    public ContatoEntity convert(){
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipoContato(this.tipoContato);
        contato.setValor(this.valor);

        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
