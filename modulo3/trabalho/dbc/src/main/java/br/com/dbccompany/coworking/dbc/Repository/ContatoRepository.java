package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

    ContatoEntity findByValor(String nome);
    Optional<ContatoEntity> findById(Integer id);
}
