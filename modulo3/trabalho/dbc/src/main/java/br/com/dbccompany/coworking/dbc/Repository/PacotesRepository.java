package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PacotesRepository extends CrudRepository<PacotesEntity, Integer> {
    PacotesEntity findByValor(Integer valor);
    Optional<PacotesEntity> findById(Integer id);
}
