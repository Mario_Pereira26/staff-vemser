package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacosEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String nome;

    @Column(nullable = false)
    private int qtdPessoas;

    @Column(nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espacos")
    @JsonManagedReference
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "espaco")
    @JsonManagedReference
    private List<SaldoClienteEntity> espacos;

    @OneToMany(mappedBy = "espacosContrato")
    @JsonManagedReference
    private List<ContratacaoEntity> contratacoes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<SaldoClienteEntity> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<SaldoClienteEntity> espacos) {
        this.espacos = espacos;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
