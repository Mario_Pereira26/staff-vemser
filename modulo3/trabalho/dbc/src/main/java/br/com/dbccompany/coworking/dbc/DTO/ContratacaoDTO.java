package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.*;

import java.util.List;

public class ContratacaoDTO {
    private int id;
    private EspacosEntity espaco;
    private ClientesEntity cliente;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int desconto;
    private int prazo;
    private List<PagamentosEntity> pagamentos;

    public ContratacaoDTO() {}

    public ContratacaoDTO(ContratacaoEntity contrato) {
        this.id = contrato.getId();
        this.espaco = contrato.getEspacosContrato();
        this.cliente = contrato.getClientes();
        this.tipoContratacao = contrato.getTipoContratacao();
        this.quantidade = contrato.getQuantidade();
        this.desconto = contrato.getDesconto();
        this.prazo = contrato.getPrazo();
        this.pagamentos = contrato.getPagamentos();
    }

    public ContratacaoEntity convert(){
        ContratacaoEntity contrato = new ContratacaoEntity();
        contrato.setId(this.id);
        contrato.setEspacosContrato(this.espaco);
        contrato.setClientes(this.cliente);
        contrato.setTipoContratacao(this.tipoContratacao);
        contrato.setQuantidade(this.quantidade);
        contrato.setDesconto(this.desconto);
        contrato.setPrazo(this.prazo);
        contrato.setPagamentos(this.pagamentos);
        return contrato;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public TipoContratacaoEnum getTipo() {
        return tipoContratacao;
    }

    public void setTipo(TipoContratacaoEnum tipo) {
        this.tipoContratacao = tipo;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
