package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClientesEntityId;
import br.com.dbccompany.coworking.dbc.Service.SaldoClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/saldoCliente") // rota
public class SaldoClienteController extends AbstractController<SaldoClienteService, SaldoClienteEntity, SaldoClientesEntityId> {
}
