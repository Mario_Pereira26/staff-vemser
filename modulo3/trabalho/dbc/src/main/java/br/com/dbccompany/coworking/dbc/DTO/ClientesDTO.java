package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.*;

import java.sql.Date;
import java.util.List;

public class ClientesDTO {

    private Integer id;
    private String nome;
    private String cpf;
    private Date dataNascimento;
    private List<ContatoEntity> contatos;
    private List<SaldoClienteEntity> saldosClientes;
    private List<ClientesPacotesEntity> clientesPacotes;
    private List<ContratacaoEntity> contratos;

    public ClientesDTO() {}

    public ClientesDTO(ClientesEntity cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getDataNascimento();
        this.contatos = cliente.getContatos();
        this.saldosClientes = cliente.getClientes();
        this.clientesPacotes = cliente.getClientesPacotes();
        this.contratos = cliente.getContratos();
    }

    public ClientesEntity convert(){
        ClientesEntity cliente = new ClientesEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf);
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setContatos(contatos);
        cliente.setClientes(this.saldosClientes);
        cliente.setClientesPacotes(this.clientesPacotes);
        cliente.setContratos(this.contratos);
        return cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getContratos() {
        return contratos;
    }

    public void setContratos(List<ContratacaoEntity> contratos) {
        this.contratos = contratos;
    }
}
