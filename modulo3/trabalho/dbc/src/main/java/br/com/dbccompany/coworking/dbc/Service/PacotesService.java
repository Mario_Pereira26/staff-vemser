package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Repository.PacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class PacotesService extends ServiceAbstract<PacotesRepository, PacotesEntity, Integer> {

}
