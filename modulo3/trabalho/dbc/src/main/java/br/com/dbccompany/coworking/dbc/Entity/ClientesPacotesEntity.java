package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CLIENTES_PACOTES")
public class ClientesPacotesEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_CLIENTES")
    private ClientesEntity clientesPacotes;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_PACOTES")
    private PacotesEntity pacotesCli;

    @OneToMany(mappedBy = "clientesPacote")
    @JsonManagedReference
    private List<PagamentosEntity> pagamentos;

    private int quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public PacotesEntity getPacotesCli() {
        return pacotesCli;
    }

    public void setPacotesCli(PacotesEntity pacotesCli) {
        this.pacotesCli = pacotesCli;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
