package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
@Controller
@RequestMapping("/api/contratacao") // rota
public class ContratacaoController {
    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContratacaoEntity> todosContratacao() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO contrato) throws NoSuchAlgorithmException {
        ContratacaoEntity contratacaoEntity = contrato.convert();
        ContratacaoDTO newDto = new ContratacaoDTO(service.salvar(contratacaoEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ContratacaoEntity contratoEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContratacaoEntity editarContrato(@PathVariable Integer id, @RequestBody ContratacaoEntity contrato){
        return service.editar(contrato, id);
    }
}
