package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Service.PacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/pacotes") // rota
public class PacotesController extends AbstractController<PacotesService, PacotesEntity,Integer> {
}
