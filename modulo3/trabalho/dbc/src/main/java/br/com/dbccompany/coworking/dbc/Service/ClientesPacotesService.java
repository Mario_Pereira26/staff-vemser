package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.dbc.Repository.ClientesPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientesPacotesService extends ServiceAbstract<ClientesPacotesRepository, ClientesPacotesEntity, Integer> {
}
