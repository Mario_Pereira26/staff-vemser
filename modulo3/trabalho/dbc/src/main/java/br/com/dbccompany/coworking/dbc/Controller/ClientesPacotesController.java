package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.ClientesPacotesDTO;
import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.dbc.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes") // rota
public class ClientesPacotesController {
    @Autowired
    ClientesPacotesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesPacotesEntity> todosPais() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesPacotesDTO salvar(@RequestBody ClientesPacotesDTO pais) throws NoSuchAlgorithmException {
        ClientesPacotesEntity clientePacoteEntity = pais.convert();
        ClientesPacotesDTO newDto = new ClientesPacotesDTO(service.salvar(clientePacoteEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesPacotesEntity clientePacotesEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotesEntity editarPais(@PathVariable Integer id, @RequestBody ClientesPacotesEntity clientePacote){
        return service.editar(clientePacote, id);
    }
}
