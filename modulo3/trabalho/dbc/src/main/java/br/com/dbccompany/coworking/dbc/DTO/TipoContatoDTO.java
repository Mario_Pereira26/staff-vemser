package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.TipoContatoEntity;

public class TipoContatoDTO {

    private Integer id;
    private String nome;

    public TipoContatoDTO(){}

    public TipoContatoDTO(TipoContatoEntity tipoContato) {
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
    }

    public TipoContatoEntity convert(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);

        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
