package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.AcessosEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClienteEntity;

import java.sql.Date;
import java.util.Calendar;

public class AcessosDTO {

    private int id;
    private SaldoClienteEntity clienteSaldoCliente;
    private boolean isEntrada;
    private Date data;
    private boolean isExcecao;

    public AcessosDTO(){}

    public AcessosDTO(AcessosEntity acesso){
        this.id = acesso.getId();
        this.clienteSaldoCliente = acesso.getSaldoCliente();
        this.isEntrada = acesso.isEntrada();
        if(acesso.getData() == null){
            acesso.setData((Date) Calendar.getInstance().getTime());
        }
        this.data = acesso.getData();
        this.isExcecao = acesso.isExcecao();
    }

    public AcessosEntity convert(){
        AcessosEntity acesso = new AcessosEntity();

        acesso.setId(this.id);
        acesso.setSaldoCliente(this.clienteSaldoCliente);
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setExcecao(this.isExcecao);

        return acesso;
    }

    public SaldoClienteEntity getClienteSaldoCliente() {
        return clienteSaldoCliente;
    }

    public void setClienteSaldoCliente(SaldoClienteEntity clienteSaldoCliente) {
        this.clienteSaldoCliente = clienteSaldoCliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}

