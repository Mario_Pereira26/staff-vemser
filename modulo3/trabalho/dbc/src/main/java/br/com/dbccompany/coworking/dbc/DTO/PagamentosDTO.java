package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.dbc.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoPagamentoEnum;

public class PagamentosDTO {

    private Integer id;
    private ClientesPacotesEntity clientePacote;
    private ContratacaoEntity contratacao;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentosDTO() {}

    public PagamentosDTO(PagamentosEntity pagamento) {
        this.id = pagamento.getId();
        this.clientePacote = pagamento.getClientesPacote();
        this.contratacao = pagamento.getContratacao();
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public PagamentosEntity convert() {
        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setId(this.id);
        pagamento.setClientesPacote(this.clientePacote);
        pagamento.setContratacao(this.contratacao);
        pagamento.setTipoPagamento(this.tipoPagamento);

        return pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotesEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientesPacotesEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
