package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.dbc.DbcApplication;
import br.com.dbccompany.coworking.dbc.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.dbc.Repository.UsuarioRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.security.NoSuchAlgorithmException;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    private Logger logger = LoggerFactory.getLogger(DbcApplication.class);

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity salvar(UsuarioEntity usuario) throws NoSuchAlgorithmException {
        String senha = usuario.getSenha();
        logger.info("Senha deve ser igual ou maior que 6 caracteres!");
        if(senha.length() >= 6){
            try {
                usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
                return repository.save(usuario);
            }catch (Exception e){
                logger.error("Erro ao salvar Usuario: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        logger.warn("Senha menor que 6 caracteres!");
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity trocarSenha(UsuarioEntity usuario, Integer id, String senhaNova) throws NoSuchAlgorithmException {
        logger.info("Senha deve ser igual ou maior que 6 caracteres!");
        if(senhaNova.length() >= 6){
            try {
                usuario.setId(id);
                usuario.setSenha(new BCryptPasswordEncoder().encode(senhaNova));
                return repository.save(usuario);
            }catch (Exception e){
                logger.error("Erro ao editar Usuario: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        logger.warn("Nova senha menor que 6 caracteres!");
        return null;
    }


    @Transactional(rollbackFor = Exception.class)
    public List<UsuarioDTO> retornarListaUsuarios(){
        logger.info("Criando lista de usuarios!");
        List<UsuarioDTO> listaDTO = new ArrayList<>();
        for (UsuarioEntity usuario : this.todos()){
            listaDTO.add(new UsuarioDTO(usuario));
        }
        try {
            return listaDTO;
        }catch (Exception e){
            logger.error("Ocorreu um erro na listagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean login(String login, String senha) throws NoSuchAlgorithmException {
        logger.info("busca usuario com login e senha!");
        UsuarioEntity usuario = repository.findByLoginAndSenha(login, new BCryptPasswordEncoder().encode(senha));
        return usuario == null ? false : true;
    }
}
