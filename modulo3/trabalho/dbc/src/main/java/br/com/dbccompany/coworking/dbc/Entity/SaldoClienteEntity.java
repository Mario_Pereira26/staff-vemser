package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
public class SaldoClienteEntity extends EntityAbstract<SaldoClientesEntityId> {

    @EmbeddedId
    private SaldoClientesEntityId id;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_CLIENTE", nullable = false,insertable = false,updatable = false)
    private ClientesEntity cliente;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_ESPACOS", nullable = false,insertable = false,updatable = false)
    private EspacosEntity espaco;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @Column(nullable = false)
    private Date vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    @JsonManagedReference
    private List<AcessosEntity> acessos;

    public SaldoClientesEntityId getId() {
        return id;
    }

    public void setId(SaldoClientesEntityId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }
}
