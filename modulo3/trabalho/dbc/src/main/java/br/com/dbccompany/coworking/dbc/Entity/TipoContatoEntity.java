package br.com.dbccompany.coworking.dbc.Entity;
import javax.persistence.*;
import javax.persistence.SequenceGenerator;
import java.util.List;

@Entity

public class TipoContatoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(unique = true, nullable = false)
    private String nome;

    @OneToMany(mappedBy = "tipoContato")
    private List<ContatoEntity> contatos;

    @Override
    public Integer getId() {
        return id;
    }
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
