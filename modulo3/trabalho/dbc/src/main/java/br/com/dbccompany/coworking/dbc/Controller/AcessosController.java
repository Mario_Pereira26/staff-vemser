package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.DTO.AcessosDTO;
import br.com.dbccompany.coworking.dbc.Entity.AcessosEntity;
import br.com.dbccompany.coworking.dbc.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
@Controller
@RequestMapping("/api/acessos") // rota
public class AcessosController {
    @Autowired
    AcessosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<AcessosEntity> todosAcessos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public AcessosDTO salvar(@RequestBody AcessosDTO acesso) throws NoSuchAlgorithmException {
        AcessosEntity acessoEntity = acesso.convert();
        AcessosDTO newDto = new AcessosDTO(service.salvar(acessoEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public AcessosEntity acessoEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public AcessosEntity editarAcesso(@PathVariable Integer id, @RequestBody AcessosEntity acesso){
        return service.editar(acesso, id);
    }

}
