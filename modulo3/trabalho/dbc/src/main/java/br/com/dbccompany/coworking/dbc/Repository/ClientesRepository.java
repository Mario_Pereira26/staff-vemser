package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {
    ClientesEntity findByCpf(String cpf);
    ClientesEntity findByNomeAndCpf(String nome, String cpf);
    Optional<ClientesEntity> findById(Integer id);
    List<ClientesEntity> findAllByDataNascimento(Date dataNascimento);
}
