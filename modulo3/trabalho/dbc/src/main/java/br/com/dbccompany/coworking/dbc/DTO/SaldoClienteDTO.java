package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.dbc.Entity.SaldoClientesEntityId;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;

import java.sql.Date;

public class SaldoClienteDTO {

    private SaldoClientesEntityId id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Date vencimento;
    private EspacosEntity espaco;

    public SaldoClienteDTO(){}

    public SaldoClienteDTO(SaldoClienteEntity saldo) {
        this.id = saldo.getId();
        this.tipoContratacao = saldo.getTipoContratacao();
        this.quantidade = saldo.getQuantidade();
        this.vencimento = saldo.getVencimento();
        this.espaco = saldo.getEspaco();
    }

    public SaldoClienteEntity convert(){
        SaldoClienteEntity saldo = new SaldoClienteEntity();
        saldo.setId(this.id);
        saldo.setTipoContratacao(this.tipoContratacao);
        saldo.setQuantidade(this.quantidade);
        saldo.setVencimento(this.vencimento);
        saldo.setEspaco(this.espaco);
        return saldo;
    }

    public SaldoClientesEntityId getId() {
        return id;
    }

    public void setId(SaldoClientesEntityId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }
}
