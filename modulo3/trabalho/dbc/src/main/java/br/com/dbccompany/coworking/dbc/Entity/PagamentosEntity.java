package br.com.dbccompany.coworking.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class PagamentosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_CLIENTES_PACOTES")
    private ClientesPacotesEntity clientesPacote;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @Enumerated( EnumType.STRING )
    @Column(name = "TIPO_PAGAMENTO", nullable = false)
    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotesEntity getClientesPacote() {
        return clientesPacote;
    }

    public void setClientesPacote(ClientesPacotesEntity clientesPacote) {
        this.clientesPacote = clientesPacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
