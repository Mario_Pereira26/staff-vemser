package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.Entity.ContatoEntity;
import br.com.dbccompany.coworking.dbc.Repository.ContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContatoService extends ServiceAbstract<ContatoRepository, ContatoEntity,Integer> {
}
