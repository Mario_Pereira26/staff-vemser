package br.com.dbccompany.coworking.dbc.Entity;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
