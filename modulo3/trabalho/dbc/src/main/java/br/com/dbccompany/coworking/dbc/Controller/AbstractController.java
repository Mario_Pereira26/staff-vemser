package br.com.dbccompany.coworking.dbc.Controller;

import br.com.dbccompany.coworking.dbc.Service.ServiceAbstract;
import br.com.dbccompany.coworking.dbc.Entity.EntityAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public abstract class AbstractController <S extends ServiceAbstract, E extends EntityAbstract, T> {
    @Autowired
    S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<E> todos() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public E salvar(@RequestBody E entidade) throws NoSuchAlgorithmException {
        return (E) service.salvar(entidade);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public E especifico(@PathVariable T id){
        return (E)service.porId(id);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public E editar(@PathVariable T id, @RequestBody E entidade){
        return (E)service.editar(entidade,id);
    }
}
