package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DbcApplication;
import br.com.dbccompany.coworking.dbc.Entity.*;
import br.com.dbccompany.coworking.dbc.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

@Service
public class PagamentosService extends ServiceAbstract<PagamentosRepository, PagamentosEntity, Integer> {

    private Logger logger = LoggerFactory.getLogger(DbcApplication.class);

    @Autowired
    private SaldoClienteService saldoClientesService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity salvar(PagamentosEntity pagamento){

        ContratacaoEntity contratacao = pagamento.getContratacao();
        ClientesPacotesEntity clientesPacotes = pagamento.getClientesPacote();

        if(contratacao != null){
            try{
                saldoClientesService.salvar(contratacao);
            }catch (Exception e){
                logger.error("Erro em salvar contratação do pagamento: " + e.getMessage());
                throw new RuntimeException();
            }
        }
        if(clientesPacotes != null){
            List<EspacosPacotesEntity> espacosPacotes = clientesPacotes.getPacotesCli().getEspacosPacotes();
            logger.warn("Iniciando salvar contratacao de cada saldoClienteService!");
            for(EspacosPacotesEntity espacoPacote : espacosPacotes) {
                contratacao.setEspacosContrato(espacoPacote.getEspacos());
                try {
                    saldoClientesService.salvar(contratacao);
                }catch (Exception e){
                    logger.error("Erro no salvamento do espacoPacote id: " + espacoPacote.getId() + ", " + e.getMessage());
                    throw new RuntimeException();
                }
            }
        }
        logger.warn("Gerando contratacao!");
        try {
            return repository.save(pagamento);
        }catch (Exception e){
            logger.error("Erro ao gerar contratacao: " + e.getMessage());
            throw new RuntimeException();
        }

    }
}
