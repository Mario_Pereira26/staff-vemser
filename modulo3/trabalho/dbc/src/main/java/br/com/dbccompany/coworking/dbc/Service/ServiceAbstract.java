package br.com.dbccompany.coworking.dbc.Service;

import br.com.dbccompany.coworking.dbc.DbcApplication;
import br.com.dbccompany.coworking.dbc.Entity.EntityAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {

    Logger logger = LoggerFactory.getLogger(DbcApplication.class);

    @Autowired
    R repository;

    @Transactional( rollbackFor = Exception.class )
    public E salvar(E entidade) throws NoSuchAlgorithmException {
        logger.warn("Entidade deve ter todos os campos inseridos!");
        try {
            return repository.save(entidade);
        }catch (Exception e){
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public E editar( E entidade, T id ) {
        try {
            logger.info("Para editar deve conter id da entidade, assim como todos os outros parametros!");
            entidade.setId(id);
            return repository.save(entidade);
        }catch (Exception e){
            logger.error("Erro na edição da entidade! " + e.getMessage());
            throw new RuntimeException();
        }

    }

    public E buscar(T id){
        Optional<E> entidade = repository.findById(id);
        if(entidade == null){
            return null;
        }
        try {
            return entidade.get();
        }catch (Exception e){
            logger.error("Erro em buscar entidade por (id)! " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public List<E> todos() {
        try {
            return (List<E>) repository.findAll();
        }catch (Exception e){
            logger.error("Erro em buscar todas entidades! " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public E porId( T id ){
        return repository.findById(id).get();
    }
}
