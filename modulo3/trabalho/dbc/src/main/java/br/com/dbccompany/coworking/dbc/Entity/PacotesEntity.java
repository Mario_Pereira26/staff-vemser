package br.com.dbccompany.coworking.dbc.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacotesEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private int valor;

    @OneToMany(mappedBy = "pacotes")
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "pacotesCli")
    private List<ClientesPacotesEntity> clientes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotesEntity> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClientesPacotesEntity> clientes) {
        this.clientes = clientes;
    }
}
