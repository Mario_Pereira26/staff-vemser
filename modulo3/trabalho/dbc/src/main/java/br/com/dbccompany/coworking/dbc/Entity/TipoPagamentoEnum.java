package br.com.dbccompany.coworking.dbc.Entity;

public enum TipoPagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
