package br.com.dbccompany.coworking.dbc.DTO;

import br.com.dbccompany.coworking.dbc.Entity.EspacosEntity;
import br.com.dbccompany.coworking.dbc.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.PacotesEntity;
import br.com.dbccompany.coworking.dbc.Entity.TipoContratacaoEnum;

public class EspacosPacotesDTO {

    private int id;
    private EspacosEntity espacos;
    private PacotesEntity pacotes;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int prazo;
    private EspacosEntity espaco;
    private PacotesEntity pacote;

    public EspacosPacotesDTO() {}

    public EspacosPacotesDTO(EspacosPacotesEntity espacoPacote) {
        this.id = espacoPacote.getId();
        this.espacos = espacoPacote.getEspacos();
        this.pacotes = espacoPacote.getPacotes();
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
        this.espaco = espacoPacote.getEspacos();
        this.pacote = espacoPacote.getPacotes();
    }

    public EspacosPacotesEntity convert(){
        EspacosPacotesEntity espacoPacotes = new EspacosPacotesEntity();
        espacoPacotes.setId(this.id);
        espacoPacotes.setEspacos(this.espacos);
        espacoPacotes.setPacotes(this.pacotes);
        espacoPacotes.setTipoContratacao(this.tipoContratacao);
        espacoPacotes.setQuantidade(this.quantidade);
        espacoPacotes.setPrazo(this.prazo);
        espacoPacotes.setEspacos(this.espaco);
        espacoPacotes.setPacotes(this.pacote);
        return espacoPacotes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipo) {
        this.tipoContratacao = tipo;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

}
