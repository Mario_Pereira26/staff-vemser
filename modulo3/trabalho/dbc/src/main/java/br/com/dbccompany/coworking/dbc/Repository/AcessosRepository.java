package br.com.dbccompany.coworking.dbc.Repository;

import br.com.dbccompany.coworking.dbc.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {
    List<AcessosEntity> findAllBySaldoCliente(SaldoClienteEntity saldoCliente);
}
