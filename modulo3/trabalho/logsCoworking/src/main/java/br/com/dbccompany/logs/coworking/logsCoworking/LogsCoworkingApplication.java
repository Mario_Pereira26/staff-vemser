package br.com.dbccompany.logs.coworking.logsCoworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogsCoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogsCoworkingApplication.class, args);
	}

}
