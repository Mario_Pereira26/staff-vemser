package br.com.dbccompany.logs.coworking.logsCoworking.Controller;

import br.com.dbccompany.logs.coworking.logsCoworking.DTO.LogAplicacaoDTO;
import br.com.dbccompany.logs.coworking.logsCoworking.LogsCoworkingApplication;
import br.com.dbccompany.logs.coworking.logsCoworking.Service.LogAplicacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/apiLogs")
public class LogAplicacaoController {
    // save, listartodos, especifico
    @Autowired
    LogAplicacaoService logsService;

    private Logger logger = LoggerFactory.getLogger(LogsCoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogAplicacaoDTO> todosLogs() {
        logger.info("Buscar os logs da aplicação.");
        logger.warn("Não havendo cadastros poderá retornar vazia!");
        return logsService.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public LogAplicacaoDTO salvar(@RequestBody LogAplicacaoDTO logsDTO){
        logger.info("Adicionando novo log na aplicação.");
        return logsService.salvarLog(logsDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public LogAplicacaoDTO espacoEspecifico(@PathVariable String id) {
        logger.info("Buscando o log de id: " + id);
        return logsService.porId(id);
    }
}
