package br.com.dbccompany.logs.coworking.logsCoworking.Service;

import br.com.dbccompany.logs.coworking.logsCoworking.DTO.LogAplicacaoDTO;
import br.com.dbccompany.logs.coworking.logsCoworking.Entity.LogAplicacaoEntity;
import br.com.dbccompany.logs.coworking.logsCoworking.LogsCoworkingApplication;
import br.com.dbccompany.logs.coworking.logsCoworking.Repository.LogAplicacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogAplicacaoService {
    @Autowired
    LogAplicacaoRepository logsRepository;

    private Logger logger = LoggerFactory.getLogger(LogsCoworkingApplication.class);

    @Transactional
    public List<LogAplicacaoDTO> todos() {
        try {
            List<LogAplicacaoDTO> logsDTOS = new ArrayList<>();
            for (LogAplicacaoEntity logEntity: logsRepository.findAll()) {
                logsDTOS.add(new LogAplicacaoDTO(logEntity));
            }
            return logsDTOS;
        } catch (Exception e) {
            logger.error("Erro ao listar logs: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    @Transactional
    public LogAplicacaoDTO salvarLog(LogAplicacaoDTO logAplicacaoDTO) {
        try {
            LogAplicacaoEntity logFinal = logAplicacaoDTO.convert();
            LogAplicacaoDTO newDTO = new LogAplicacaoDTO(logsRepository.save(logFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar log: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public LogAplicacaoDTO porId(String id) {
        try {
            return new LogAplicacaoDTO(logsRepository.findById(id).get());
        } catch (Exception e) {
            logger.error("Log de id: " + id +" não foi encontrado. Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
