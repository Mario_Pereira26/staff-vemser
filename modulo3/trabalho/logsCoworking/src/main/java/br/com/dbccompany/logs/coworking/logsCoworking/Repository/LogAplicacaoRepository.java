package br.com.dbccompany.logs.coworking.logsCoworking.Repository;

import br.com.dbccompany.logs.coworking.logsCoworking.Entity.LogAplicacaoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogAplicacaoRepository extends MongoRepository<LogAplicacaoEntity, String> {
    List<LogAplicacaoEntity> findByCodigo(String codigo);
    List<LogAplicacaoEntity> findByMensagem(String mensagem);
    List<LogAplicacaoEntity> findByCodigoAndMensagem(String codigo, String mensagem);
}
