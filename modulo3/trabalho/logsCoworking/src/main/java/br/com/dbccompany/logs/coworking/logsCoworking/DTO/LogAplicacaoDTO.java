package br.com.dbccompany.logs.coworking.logsCoworking.DTO;

import br.com.dbccompany.logs.coworking.logsCoworking.Entity.LogAplicacaoEntity;

public class LogAplicacaoDTO {

    private String id;
    private String codigo;
    private String mensagem;

    public LogAplicacaoDTO() {}

    public LogAplicacaoDTO(LogAplicacaoEntity logAplicacao) {
        this.id = logAplicacao.getId();
        this.codigo = logAplicacao.getCodigo();
        this.mensagem = logAplicacao.getMensagem();
    }

    @Override
    public String toString() {
        return String.format(
                "Log[id=%s, cogigo='%s', mensagem='%s']",
                id, codigo, mensagem);
    }

    public LogAplicacaoEntity convert() {
        LogAplicacaoEntity logAplicacaoEntity = new LogAplicacaoEntity();
        logAplicacaoEntity.setId(this.id);
        logAplicacaoEntity.setCodigo(this.codigo);
        logAplicacaoEntity.setMensagem(this.mensagem);
        return logAplicacaoEntity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

}
