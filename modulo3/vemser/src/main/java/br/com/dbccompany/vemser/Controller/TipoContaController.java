package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Service.TipoContaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/tipoConta") // rota
public class TipoContaController extends AbstractController<TipoContaService, TipoContaEntity,Integer> {
}
