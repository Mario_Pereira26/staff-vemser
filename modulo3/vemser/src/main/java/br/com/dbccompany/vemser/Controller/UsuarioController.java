package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Service.UsuarioService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/usuario") // rota
public class UsuarioController extends AbstractController<UsuarioService, UsuarioEntity, Integer>{
}
