package br.com.dbccompany.vemser.Entity;
import javax.persistence.*;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "id")// herança
public class GerenteEntity extends UsuarioEntity {

    private int codigo;

    @Enumerated( EnumType.STRING )
    private TipoGerenteEnum tipoGerente;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable(name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn(name = "ID_GERENTE") },
            inverseJoinColumns = { @JoinColumn(name = "ID_CONTA"), @JoinColumn(name = "ID_TIPO_CONTA") }
    )
    private List<ContaEntity> contas;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public TipoGerenteEnum getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerenteEnum tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}
