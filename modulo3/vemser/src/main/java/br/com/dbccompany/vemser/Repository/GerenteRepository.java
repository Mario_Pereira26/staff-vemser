package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GerenteRepository extends CrudRepository<GerenteEntity, Integer> {
}
