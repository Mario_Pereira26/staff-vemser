package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.DTO.GeralDTO;
import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/api/pais") // rota
public class PaisController {
    @Autowired
    PaisService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PaisEntity> todosPais() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PaisDTO salvar(@RequestBody PaisDTO pais){
        PaisEntity paisEntity = pais.convert();
        PaisDTO newDto = new PaisDTO(service.salvar(paisEntity));
        return newDto;
    }
    // ex: localhost:8080/api/pais/ver/1
    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PaisEntity especifico(@PathVariable Integer id){
        return (PaisEntity)service.porId(id);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public PaisEntity editar(@PathVariable Integer id, @RequestBody PaisEntity entidade){
        return (PaisEntity)service.editar(entidade,id);
    }

}
