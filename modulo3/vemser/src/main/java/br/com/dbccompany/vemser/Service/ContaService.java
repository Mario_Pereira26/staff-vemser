package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Repository.ContaRepository;
import org.springframework.stereotype.Service;


@Service
public class ContaService extends ServiceAbstract<ContaRepository, ContaEntity, ContaEntityId> {

}
