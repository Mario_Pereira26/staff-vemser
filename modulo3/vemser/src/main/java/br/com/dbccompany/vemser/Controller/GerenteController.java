package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Service.GerenteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/gerente") // rota
public class GerenteController extends AbstractController<GerenteService, GerenteEntity,Integer> {
}
