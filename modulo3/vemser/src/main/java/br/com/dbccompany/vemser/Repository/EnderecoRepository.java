package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {
    EnderecoEntity save(EnderecoEntity entidade);
}
