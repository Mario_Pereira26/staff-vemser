package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import br.com.dbccompany.vemser.Service.ContaClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/contaCliente") // rota
public class ContaClienteController extends AbstractController<ContaClienteService, ContaClienteEntity, ContaClienteId> {
}
