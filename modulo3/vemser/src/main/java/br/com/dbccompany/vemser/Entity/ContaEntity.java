package br.com.dbccompany.vemser.Entity;
import javax.persistence.*;
import java.util.List;

@Entity
public class ContaEntity extends EntityAbstract<ContaEntityId> {

    @EmbeddedId
    private ContaEntityId id;

    private String codigo;
    private double saldo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_AGENCIA")
    private AgenciaEntity agencia;

    @ManyToOne
    @MapsId("ID_TIPO_CONTA")
    @JoinColumn(name = "ID_TIPO_CONTA", nullable = false)
    private TipoContaEntity tipoConta;

    @OneToMany(mappedBy = "conta")
    private List<MovimentacaoEntity> movimetacoes;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn(name = "ID_CONTA"), @JoinColumn(name = "ID_TIPO_CONTA") },
            inverseJoinColumns = { @JoinColumn(name = "ID_GERENTE") }
    )
    private List<GerenteEntity> gerentes;

    @OneToMany(mappedBy = "conta")
    private List<ContaClienteEntity> contasClientes;

    public ContaEntityId getId() {
        return id;
    }

    public void setId(ContaEntityId id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public TipoContaEntity getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoContaEntity tipoConta) {
        this.tipoConta = tipoConta;
    }

    public List<MovimentacaoEntity> getMovimetacoes() {
        return movimetacoes;
    }

    public void setMovimetacoes(List<MovimentacaoEntity> movimetacoes) {
        this.movimetacoes = movimetacoes;
    }

    public List<GerenteEntity> getGerentes() {
        return gerentes;
    }

    public void setGerentes(List<GerenteEntity> gerentes) {
        this.gerentes = gerentes;
    }

    public List<ContaClienteEntity> getContasClientes() {
        return contasClientes;
    }

    public void setContasClientes(List<ContaClienteEntity> contasClientes) {
        this.contasClientes = contasClientes;
    }

}
