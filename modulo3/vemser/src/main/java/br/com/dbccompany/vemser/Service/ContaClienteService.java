package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import br.com.dbccompany.vemser.Repository.ContaClienteRepository;
import org.springframework.stereotype.Service;

@Service
public class ContaClienteService extends ServiceAbstract<ContaClienteRepository, ContaClienteEntity, ContaClienteId> {

}
