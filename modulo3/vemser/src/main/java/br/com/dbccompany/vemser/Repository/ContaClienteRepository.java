package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaClienteRepository extends CrudRepository<ContaClienteEntity, ContaClienteId> {
}
