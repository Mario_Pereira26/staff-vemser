package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Service.BancoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/banco") // rota
public class BancoController extends AbstractController<BancoService, BancoEntity,Integer> {
}
