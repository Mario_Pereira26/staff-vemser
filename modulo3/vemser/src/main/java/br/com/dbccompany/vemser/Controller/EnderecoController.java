package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/endereco") // rota
public class EnderecoController extends AbstractController<EnderecoService, EnderecoEntity,Integer> {

}
