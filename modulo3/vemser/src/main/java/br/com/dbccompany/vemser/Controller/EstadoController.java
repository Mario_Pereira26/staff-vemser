package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Service.EstadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/estado") // rota
public class EstadoController extends AbstractController<EstadoService, EstadoEntity, Integer> {
}
